﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GiantAttackState : State
{
    [Space(10)]
    public NewIdleState newIdleState;
    public ChaseState chaseState;
    [Space(10)]
    [SerializeField] Transform lookPoint;
    [SerializeField] int attackNumber;

    [Space(10)]
    [Header("Jump Slam Attack")]
    [SerializeField] float attackDistance;
    [SerializeField] Transform jumpPoint;
    [SerializeField] GameObject jumpSlamEffect;
    
    [Space(10)]
    [Header("RockThrow Attack")]
    [SerializeField] float projectileForce;
    [SerializeField] Transform firePoint;
    [SerializeField] GameObject rockPrefab;

    [Space(10)]
    [Header("RunAndSlam Attack")]
    [SerializeField] GameObject runAndSlamCollider;

    float _distance;
    float _directionMultiplier;

    bool _returnToChaseState;
    bool _hasAttack;
    bool _isAttacking;
    bool _canFaceTarget;

    void Start()
    {
        _isAttacking = false;
        _returnToChaseState = false;
    }

    public override State RunCurrentState()
    {
        if (_enemyStats.isChanged)
        {
            //animator.SetBool("isWalking", false);
            return newIdleState;
        }
        if (_returnToChaseState)
        {
            _returnToChaseState = false;
            _agent.updatePosition = true;
            _hasAttack = false;
            attackNumber = 0;
            return chaseState;
        }
                
        _distance = Vector3.Distance(_target.position, transform.position);
        _directionMultiplier = _distance / 7;

        if (_canFaceTarget)
        {
            FaceTarget();
        }

        if (!_isAttacking)
        {
            //LookForPlayer();
        }

        if (!_hasAttack)
        {
            ChooseAttack();
        }

        if (_hasAttack && !_isAttacking)
        {            
            if (attackNumber == 1 && _distance < 10f)
            {
                _canFaceTarget = false;
                attackNumber = 0;
                _isAttacking = true;
                StartCoroutine(DoubleSlamAttack());                
            }
            if (attackNumber == 2 && _distance > 15f)
            {
                _canFaceTarget = false;
                attackNumber = 0;
                _isAttacking = true;
                StartCoroutine(JumpAttack());
            }
            if (attackNumber == 3 && _distance > 20f)
            {
                _canFaceTarget = false;
                attackNumber = 0;
                _isAttacking = true;
                StartCoroutine(RockAttack());
            }
            if (attackNumber == 4 && _distance > 5f && _distance < 20f)
            {
                _canFaceTarget = false;
                attackNumber = 0;
                _isAttacking = true;
                StartCoroutine(RunAndSlam());
            }
            else
            {                
                ChooseAttack();
            }
        }

        if (_returnToChaseState)
        {
            _returnToChaseState = false;
            _agent.updatePosition = true;
            _hasAttack = false;
            attackNumber = 0;
            return chaseState;
        }
        else
        {
            return this;
        }
    }

    void ChooseAttack()
    {
        _hasAttack = true;
        attackNumber = Random.Range(1, 5);
        //attackNumber = 2;
        //Debug.Log(attackNumber + " = attackNumber");
    }

    void LookForPlayer()
    {
        float lookRange = 40f;
        RaycastHit hitInfo;

        if (Physics.Raycast(lookPoint.transform.position, (_target.position - lookPoint.transform.position), out hitInfo, lookRange))
        {
            Debug.Log("Giant sees " + hitInfo.collider.name);
            if (hitInfo.transform.gameObject.CompareTag("Player"))
            {
                _returnToChaseState = false;
            }
            else
            {
                _returnToChaseState = true;
            }
        }
        else
        {
            _returnToChaseState = true;
        }
    }

    private IEnumerator DoubleSlamAttack()
    {
        //Debug.Log("pliip");
        _isAttacking = true;
        _canFaceTarget = true;
        animator.SetBool("doubleSlam", true); 

        yield return new WaitForSeconds(0.45f);

        runAndSlamCollider.SetActive(true);

        yield return new WaitForSeconds(0.10f);

        runAndSlamCollider.SetActive(false);

        yield return new WaitForSeconds(0.50f);

        animator.SetBool("doubleSlam", false);
        attackNumber = 0;
        _isAttacking = false;
        _hasAttack = false;
        _returnToChaseState = true;
    }

    private IEnumerator JumpAttack()
    {        
        _agent.SetDestination(_target.position);
        //Debug.Log("JumpAttack");
        _isAttacking = true;
        animator.SetBool("jumpSlam", true);        

        _agent.updatePosition = true;
        _agent.speed = 100f;
        _agent.acceleration = 50f;

        yield return new WaitForSeconds(1f);
        
        jumpSlamEffect.SetActive(true);
        jumpSlamEffect.GetComponentInChildren<ParticleSystem>().Play();
        jumpSlamEffect.GetComponentInChildren<Animator>().SetTrigger("slam");
        jumpSlamEffect.GetComponent<CapsuleCollider>().enabled = true;

        _agent.SetDestination(transform.position);
        _agent.speed = normalSpeed;
        _agent.acceleration = 30f;

        yield return new WaitForSeconds(0.5f);

        animator.SetBool("jumpSlam", false);        

        yield return new WaitForSeconds(1.2f);

        jumpSlamEffect.SetActive(false);
        _canFaceTarget = true;

        yield return new WaitForSeconds(0.2f);

        attackNumber = 0;
        _isAttacking = false;
        _hasAttack = false;
        _returnToChaseState = true;
    }

    private IEnumerator RockAttack()
    {
        _isAttacking = true;
        animator.SetBool("rockAttack", true);

        yield return new WaitForSeconds(0.6f);

        GameObject rock = Instantiate (rockPrefab, firePoint.position, firePoint.rotation * Quaternion.Euler(90f, 0f, 0f));
        Destroy(rock, 5);
        rock.transform.parent = firePoint.transform;
        _canFaceTarget = true;

        yield return new WaitForSeconds(0.75f);

        Vector3 t = (_target.transform.position + new Vector3(0, 3, 0) + _shootTargetMove * _directionMultiplier) - firePoint.transform.position;
        Rigidbody rb = rock.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rock.transform.parent = null;
        rb.AddForce(t.normalized * projectileForce, ForceMode.Impulse);

        yield return new WaitForSeconds(1f);

        animator.SetBool("rockAttack", false);
        attackNumber = 0;
        _isAttacking = false;
        _hasAttack = false;
        _returnToChaseState = true;
    }

    private IEnumerator RunAndSlam()
    {
        _isAttacking = true;
        _canFaceTarget = true; 

        yield return new WaitForSeconds(0.4f);

        _canFaceTarget = false;
        runAndSlamCollider.SetActive(true);
        _agent.SetDestination(jumpPoint.position);
        animator.SetBool("runSlam", true);
        _agent.speed = 50f;
        _agent.acceleration = 50f;

        yield return new WaitForSeconds(1.6f);

        animator.SetBool("runSlam", false);
        _agent.SetDestination(transform.position);
        _agent.speed = normalSpeed;
        _agent.acceleration = 30f;
        runAndSlamCollider.SetActive(false);

        yield return new WaitForSeconds(1f);

        attackNumber = 0;
        _isAttacking = false;
        _hasAttack = false;
        _returnToChaseState = true;
    }
}
