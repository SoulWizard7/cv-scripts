﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoblinAttackState : State
{
    [Space(10)]
    public GoblinHideState hideState;
    public NewIdleState newIdleState;
    bool hide;

    [SerializeField] GameObject smokeBombPrefab;
    [SerializeField] Transform hand;
    [SerializeField] Transform lookPoint;

    [SerializeField] float waitUntilAttack;
    [SerializeField] float attackAnimationDone;
    [SerializeField] float throwingForce;

    bool _isAttacking;
    bool _playerIsInView;
    bool _isInAttackState = false;

    void Start()
    {
        _isAttacking = true;
    }

    public override State RunCurrentState()
    {
        _isInAttackState = true;
        if (_enemyStats.isChanged)
        {
            _isInAttackState = false;
            animator.SetBool("isWalking", false);
            animator.SetBool("isClownface", false);
            return newIdleState;
        }
        


        if (_isAttacking && _playerIsInView)
        {
            StartCoroutine(SmokeBomb());
        }

        if (hide)
        {
            _isInAttackState = false;
            _isAttacking = true;
            hide = false;
            _playerIsInView = false;
            animator.SetBool("isClownface", false);
            return hideState;
        }
        else
        {
            return this;
        }
    }

    private void FixedUpdate()
    {
        if (_isInAttackState)
        {
            FaceTarget();

            if (!_playerIsInView)
            {
                LookForPlayer();
                animator.SetBool("isClownface", true);
            }

        }

    }

    private void LookForPlayer()
    {
        float lookRange = 40f;
        RaycastHit hitInfo;

        if (Physics.Raycast(lookPoint.transform.position, (_target.position - lookPoint.transform.position), out hitInfo, lookRange))
        {
            if (hitInfo.transform.gameObject.CompareTag("Player"))
            {
                _playerIsInView = true;
                //hide = true;
            }
            else
            {
                _playerIsInView = false;
            }
        }
        else
        {
            _playerIsInView = false;
        }
    }

    private IEnumerator SmokeBomb()
    {
        _isAttacking = false;
        _playerIsInView = true;
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("isClownface", false);
        animator.SetBool("isAttacking", true);
        GameObject bomb = Instantiate(smokeBombPrefab, hand.position, hand.rotation);
        Destroy(bomb, 7);
        bomb.transform.parent = hand.transform;
        yield return new WaitForSeconds(waitUntilAttack);
        Rigidbody rb = bomb.GetComponent<Rigidbody>();

        Vector3 t = _target.position - transform.position;
        bomb.transform.parent = null;
        rb.AddForce(t.normalized * throwingForce, ForceMode.Impulse);
        rb.useGravity = true;

        yield return new WaitForSeconds(attackAnimationDone);
        animator.SetBool("isAttacking", false);
        
        hide = true;
        //_isAttacking = true;
    }
}
