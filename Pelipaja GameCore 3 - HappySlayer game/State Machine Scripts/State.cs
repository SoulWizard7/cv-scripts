﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class State : MonoBehaviour
{
    public abstract State RunCurrentState();
    protected Animator animator;

    protected NavMeshAgent _agent;
    protected Transform _target;
    protected EnemyStats _enemyStats;
    protected Rigidbody _rb;
    protected Vector3 _shootTargetMove;
    protected PlayerHealth _playerHealth;

    //Movement Speeds *** if changed, animation speed must sync ***
    [Header("Speed")] 
    [SerializeField] public float normalSpeed;
    [SerializeField] float idleSpeed;
    [Space(10)]
    [Header("Enemy Type ***Choose one only***")]
    public bool goblin;
    public bool witch;
    public bool stoneGiant;
    public bool hornedMob;
    public float idleStopDistance;

    public bool FPS = false;
    

    // Idle wander // variables
    protected float wanderRadius = 20;
    protected float wanderTimer = 0.2f;
    protected float timer;
    protected Vector3 pos;


    protected virtual void Awake()
    {
        _agent = GetComponentInParent<NavMeshAgent>();
        _target = GameObject.FindGameObjectWithTag("TargetPoint").transform;
        _enemyStats = GetComponentInParent<EnemyStats>();
        _rb = GetComponentInParent<Rigidbody>();
        animator = GetComponentInParent<Animator>();
        //_shootTargetMove = _target.gameObject.GetComponentInParent<PlayerMovement>().GetMove();
        _playerHealth = _target.GetComponentInParent<PlayerHealth>();
    }

    protected virtual void Update()
    {
        if (FPS)
        {
            _shootTargetMove = _target.gameObject.GetComponentInParent<PlayerMovement>().GetMove();
        }

        else
        {
            _shootTargetMove = _target.gameObject.GetComponentInParent<ThirdPersonController>().GetMove();
        }

        //_target = GameObject.FindGameObjectWithTag("TargetPoint").transform;
        timer += Time.deltaTime;
    }

    #region Wandering Functions
    protected void Wandering()
    {
        float distance = Vector3.Distance(pos, transform.position);
        if (distance < idleStopDistance)
        {
            animator.SetBool("isIdleWalk", false);
        }

        if (timer >= wanderTimer && timer > 0)
        {
            FindPos();
        }

        if (_agent.pathStatus == NavMeshPathStatus.PathPartial)
        {
            FindPos();
        }
    }

    protected void FindPos()
    {
        Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
        _agent.SetDestination(newPos);
        pos = newPos;
        _agent.speed = idleSpeed;
        timer = -5;
        animator.SetBool("isIdleWalk", true);
    }

    protected static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;
        randDirection += origin;
        //NavMeshHit navHit;
        NavMesh.SamplePosition(randDirection, out NavMeshHit navHit, dist, layermask);
        return navHit.position;
    }
    #endregion

    protected void FaceTarget()
    {
        Vector3 direction = (_target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        _rb.transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10f);
    }

}
