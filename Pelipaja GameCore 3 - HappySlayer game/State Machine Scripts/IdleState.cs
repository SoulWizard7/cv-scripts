﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IdleState : State
{
    [Space(10)]
    public ChaseState witchChaseState;
    public GoblinHideState goblinHideState;
    public ChaseState giantChaseState;
    public ChaseState hornedMobChaseState;

    bool canSeeThePlayer;
    [SerializeField] float lookRadius = 20f;
    bool insideRadius;
    [SerializeField] Transform lookPoint;


    [Header("Enemy is wandering in Idle")]
    public bool isWandering;

    

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    public override State RunCurrentState()
    {
        if (isWandering)
        {
            Wandering();
        }

        float distance = Vector3.Distance(_target.position, transform.position);
        RaycastHit hitInfo;

        if (distance <= lookRadius)
        {
            insideRadius = true;
            if (Physics.Raycast(lookPoint.transform.position, (_target.position - lookPoint.transform.position), out hitInfo, lookRadius))
            {
                //Debug.Log(hitInfo.collider.name);
                if (hitInfo.transform.gameObject.CompareTag("Player"))
                {
                    Debug.Log(hitInfo.collider.name);
                    canSeeThePlayer = true;
                }
                else
                {
                    canSeeThePlayer = false;
                }
            }
        }


        if (_enemyStats.damaged)
        {
            _agent.speed = normalSpeed;
            animator.SetBool("isIdleWalk", false);
            if (goblin)
            {
                return goblinHideState;
            }
            if (witch)
            {
                return witchChaseState;
            }
            if (stoneGiant)
            {
                return giantChaseState;
            }
            if (hornedMob)
            {
                return hornedMobChaseState;
            }
        }
        if (canSeeThePlayer && insideRadius)
        {
            _agent.speed = normalSpeed;
            animator.SetBool("isIdleWalk", false); 
            if (goblin)
            {
                return goblinHideState;
            }
            if (witch)
            {
                return witchChaseState;
            }
            if (stoneGiant)
            {
                return giantChaseState;
            }
            if (hornedMob)
            {
                return hornedMobChaseState;
            }
            else
            {
                return this;
            }
        }
        else
        {
            return this;
        }
    }
}
