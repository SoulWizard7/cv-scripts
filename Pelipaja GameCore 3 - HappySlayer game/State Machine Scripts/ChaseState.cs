﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class ChaseState : State
{
    [Space(10)]
    public AttackState witchAttackState;
    public GiantAttackState giantAttackState;
    public NewIdleState newIdleState;
    public HornedMobAttackState hornedMobAttackState;
    public IdleState idleState;
    bool isInAttackRange;
    [SerializeField] float attackRadius = 10f;
    [SerializeField] float backToIdleDistance = 200f;

    [SerializeField] Transform ray1;
    [SerializeField] Transform ray2;
    bool _r1;
    bool _r2;
    bool isChasing = false;
    RaycastHit hitInfo1;
    RaycastHit hitInfo2;

    float distance;
    bool playerIsInView = false;

    public override State RunCurrentState()
    {
        if (_enemyStats.isChanged)
        {
            animator.SetBool("isWalking", false);
            isChasing = false;
            return newIdleState;
        }
        isChasing = true;
        distance = Vector3.Distance(_target.position, transform.position);
        _agent.updatePosition = true;
        /*if (distance <= 3f)
        {
            isInAttackRange = true;
        }*/

        if (distance > backToIdleDistance)
        {
            animator.SetBool("isWalking", false);
            _enemyStats.damaged = false;
            idleState.isWandering = true;
            return idleState;
        }

        _agent.SetDestination(_target.position);

        animator.SetBool("isWalking", true);

        if (_r1 && _r2)
        {
            playerIsInView = true;
        }
        else
        {
            playerIsInView = false;
        }
               

        if (distance <= attackRadius)
        {
            isInAttackRange = true;
        }

        if (isInAttackRange && playerIsInView)
        {
            isChasing = false;
            _agent.SetDestination(transform.position);
            //_agent.updatePosition = false;

            //isInAttackRange = false;
            playerIsInView = false;
            animator.SetBool("isWalking", false);
            _r1 = false;
            _r2 = false;
            if (witch)
            {
                return witchAttackState;
            }
            if (stoneGiant)
            {
                return giantAttackState;
            }
            if (hornedMob)
            {
                return hornedMobAttackState;
            }
            else
            {
                return this;
            }
        }
        else
        {
            return this;
        }
    }

    void FixedUpdate()
    {
        if (isChasing)
        {
            if (Physics.Raycast(ray1.transform.position, (_target.position - ray1.transform.position), out hitInfo1, attackRadius))
            {
                //Debug.Log("RAY1" + hitInfo1.collider.name);
                if (hitInfo1.transform.gameObject.CompareTag("Player"))
                {
                    _r1 = true;
                }
                else
                {
                    _r1 = false;
                }
            }
            if (Physics.Raycast(ray2.transform.position, (_target.position - ray2.transform.position), out hitInfo2, attackRadius))
            {
                if (hitInfo2.transform.gameObject.CompareTag("Player"))
                {
                    _r2 = true;
                }
                else
                {
                    _r2 = false;
                }
            }
        }
    }
}