﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackState : State
{
    [Space(10)]
    public ChaseState chaseState;
    public NewIdleState newIdleState;
    bool returnToChaseState;
    public GameObject witchSelf;

    [Header("Attacking")]
    public GameObject spikePrefab;
    public Transform firePoint;
    public float projectileForce;

    bool _playerIsInView;
    bool _isAttacking;
    [SerializeField] float timeToNextAttack;
    [SerializeField] Transform lookPoint;
    [SerializeField] float weaponRange = 35f;

    [Header("Teleport")]
    [SerializeField] GameObject aoeAttackPrefab;
    [SerializeField] Transform aoePoint;
    [SerializeField] GameObject teleportEffectPrefab;
    [SerializeField] GameObject teleportTrailPrefab;
    [SerializeField] float teleportRadius = 20;
    [SerializeField] float maxHidingSpotSearchRange = 200f;

    float _directionMultiplier;
    float _distance;
    float teleportCooldown;
    bool canTeleport = true;
    bool isTeleporting = false;
    


    public override State RunCurrentState()
    {
        if (_enemyStats.isChanged)
        {
            return newIdleState;
        }
        teleportCooldown -= Time.deltaTime;
        if(teleportCooldown < 1 && teleportCooldown > 0)
        {
            canTeleport = true;
        }
                        
        _distance = Vector3.Distance(_target.position, transform.position);

        _directionMultiplier = _distance / 7;

        if (!_isAttacking && _distance < teleportRadius && canTeleport)
        {
            _isAttacking = true;
            canTeleport = false;
            teleportCooldown = 17f;
            StartCoroutine(TeleportAttack());
        }

        if (!_isAttacking)
        {
            ShootSpike();
        }

        if (isTeleporting)
        {
            Teleport();
        }

        FaceTarget();

        if (_playerIsInView && !_isAttacking)
        {
            StartCoroutine(SpikeAttack());
        }

        if (returnToChaseState)
        {
            returnToChaseState = false;
            return chaseState;
        }
        else
        {
            return this;
        }
    }

    
    void ShootSpike()
    {
        
        RaycastHit hitInfo;
        
        if (Physics.Raycast(lookPoint.transform.position, (_target.position - lookPoint.transform.position), out hitInfo, weaponRange))
        {
            //Debug.Log(hitInfo.collider.name);
            //Debug.DrawRay(lookPoint.transform.position, (_target.position - lookPoint.transform.position), Color.green, 0.5f);
            if (hitInfo.transform.gameObject.CompareTag("Player"))
            {
                _playerIsInView = true;
            }
            else
            {
                _agent.updatePosition = true;
                returnToChaseState = true;
            }
        }
        else
        {
            _agent.updatePosition = true;
            returnToChaseState = true;
        }        
        #region BoxCast test
        /*
        if (Physics.BoxCast(firePoint.transform.position, new Vector3(1,1,1), (_target.position - transform.position), out hitInfo, firePoint.transform.rotation, weaponRange))
        {
            if (!hitInfo.transform.gameObject.CompareTag("Player"))
            {
                returnToChaseState = true;
            }
            else if (hitInfo.transform.gameObject.CompareTag("Player"))
            {
                playerIsInView = true;
            }
            else
            {                
                returnToChaseState = true;
            }
        }*/
        #endregion
    }

    private IEnumerator SpikeAttack()
    {
        _isAttacking = true;

        animator.SetBool("isAttacking", true);
        yield return new WaitForSeconds(0.5f);
        firePoint.gameObject.GetComponent<ParticleSystem>().Play();

        yield return new WaitForSeconds(0.5f);
        //line.enabled = true;

        Debug.Log("SpikeAttack!");

        //yield return shotDuration;

        //Vector3 t = _shootTarget - firePoint.transform.position;
        
        Vector3 t = (_target.transform.position + new Vector3(0, 0, 0) + _shootTargetMove * _directionMultiplier) - firePoint.transform.position;

        GameObject spike = Instantiate(spikePrefab, firePoint.position, firePoint.rotation * Quaternion.Euler (90f, 0f, 0f));
        //GameObject spike = Instantiate(spikePrefab, firePoint.position, Quaternion.Euler(t));
        Rigidbody rb = spike.GetComponent<Rigidbody>();

        rb.AddForce(t.normalized * projectileForce, ForceMode.Impulse);

        //rb.AddForce(firePoint.forward * projectileForce, ForceMode.Impulse);
        yield return new WaitForSeconds(0.5f);

        animator.SetBool("isAttacking", false);
        Destroy(spike, 5f);
                
        //line.enabled = false;

        yield return new WaitForSeconds(timeToNextAttack);
                
        _playerIsInView = false;
        _isAttacking = false;        
    }

    private IEnumerator TeleportAttack()
    {
        animator.SetBool("isAttackingSecondary", true);

        yield return new WaitForSeconds(0.5f);

        Instantiate(aoeAttackPrefab, aoePoint.position, aoePoint.rotation);

        yield return new WaitForSeconds(0.5f);

        animator.SetBool("isAttackingSecondary", false);

        ScaleDown();

        GameObject teleportEffect = Instantiate(teleportEffectPrefab, transform.position, transform.rotation);

        yield return new WaitForSeconds(0.3f);
        Destroy(teleportEffect, 3f);
        isTeleporting = true;
        yield return new WaitForSeconds(7f);
        _isAttacking = false;
    }

    public void Teleport()
    {
        GameObject teleportSpot;
        GameObject[] teleportSpots;
        

        teleportSpots = GameObject.FindGameObjectsWithTag("HidingSpot");
        int index = Random.Range(0, teleportSpots.Length);

        teleportSpot = teleportSpots[index];
        Debug.Log(teleportSpot.name);
        float distance = Vector3.Distance(teleportSpot.transform.position, transform.position);
        if (distance < 30f)
        {
            return;
        }
        else if (distance > maxHidingSpotSearchRange)
        {
            return;
        }
        else
        {
            //Vector3 t = teleportSpot.transform.position;
            GameObject teleportTrail = Instantiate(teleportTrailPrefab, transform.position, Quaternion.LookRotation((transform.position - teleportSpot.transform.position)) * Quaternion.Euler(0f, 90f, 0f));
            Destroy(teleportTrail, 10f);
            witchSelf.transform.position = teleportSpot.transform.position;
            _agent.Warp(transform.position);
            isTeleporting = false;
        }
    }
    public void ScaleDown()
    {
        var scaleDown = new Vector3(0f, 0f, 0f);
        StartCoroutine(ScaleDown(witchSelf, scaleDown, 0.5f));
    }

    public IEnumerator ScaleDown(GameObject objectToScale, Vector3 scaleDown, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingScale = objectToScale.transform.localScale;
        while (elapsedTime < seconds)
        {
            objectToScale.transform.localScale = Vector3.Lerp(startingScale, scaleDown, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        witchSelf.transform.localScale = new Vector3(1, 1, 1);
    }
}