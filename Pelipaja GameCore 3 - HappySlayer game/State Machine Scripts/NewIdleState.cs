﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NewIdleState : State
{
    [Space(10)]

    public ChaseState chaseState;
    public bool canSeeThePlayer;
        
    GameObject isChanged;

    void Start()
    {        
        isChanged = gameObject.transform.Find("Thumbs").gameObject;
    }


    public override State RunCurrentState()
    {
        isChanged.GetComponent<Thumb>().ShowThumb(false);
        _agent.updatePosition = true;

        Wandering();

        if (canSeeThePlayer)
        {
            return chaseState;
        }
        else
        {
            return this;
        }
    }
    
}
