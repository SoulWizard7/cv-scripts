﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HornedMobAttackState : State
{
    [Space(10)]
    public NewIdleState newIdleState;
    public ChaseState chaseState;

    [SerializeField] GameObject runAndSlamCollider;
    [SerializeField] Transform runPoint;

    bool _returnToChaseState;
    bool _canFaceTarget;
    float _distance;
    bool _isAttacking;
    bool _runPointUpdate = false;

    void Start()
    {
        
    }

    public override State RunCurrentState()
    {
        if (_enemyStats.isChanged)
        {
            //animator.SetBool("isWalking", false);
            return newIdleState;
        }
        if (_returnToChaseState)
        {
            _returnToChaseState = false;
            _agent.updatePosition = true;
            
            return chaseState;
        }

        //_agent.updatePosition = false;
        _distance = Vector3.Distance(_target.position, transform.position);
        
        if (!_isAttacking)
        {
            StartCoroutine(HornAttack());
        }


        if (_canFaceTarget)
        {
            FaceTarget();
        }

        if (_runPointUpdate)
        {
            _agent.SetDestination(runPoint.position);
        }
                

        if (_returnToChaseState)
        {
            _returnToChaseState = false;
            _agent.updatePosition = true;
            
            return chaseState;
        }
        else
        {
            return this;
        }
    }

    IEnumerator HornAttack()
    {
        _isAttacking = true;
        _canFaceTarget = true;
        animator.SetBool("isAttacking", true);
        yield return new WaitForSeconds(0.4f);

        _canFaceTarget = false;

        runAndSlamCollider.SetActive(true);
        _runPointUpdate = true;
        //_agent.SetDestination(runPoint.position);

        _agent.speed = 45f;
        _agent.acceleration = 40f;


        yield return new WaitForSeconds(2f);
        _runPointUpdate = false;
        _agent.SetDestination(transform.position);

        yield return new WaitForSeconds(0.4f);
        animator.SetBool("isAttacking", false);
        _agent.speed = normalSpeed;
        _agent.acceleration = 30f;

        runAndSlamCollider.SetActive(false);

        yield return new WaitForSeconds(1f);
        
        _isAttacking = false;
        _returnToChaseState = true;
    }

}
