﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoblinHideState : State
{
    [Space(10)]
    public GoblinAttackState attackState;
    public NewIdleState newIdleState;
    public IdleState idleState;
    bool isInAttackMode;

    [SerializeField] float maxHidingSpotSearchRange = 100f;
    [SerializeField] float backToIdleDistance = 200f;

    [SerializeField] Transform lookPoint;

    GameObject hidingSpot;
    GameObject[] hidingSpots;
    bool needsHidingSpot;
    bool isHiding;

    bool _scaredAttack;
    float reloadTimer;

    void Start()
    {
        needsHidingSpot = true;
    }

    public override State RunCurrentState()
    {
        //Debug.Log(_agent.pathStatus);
        if (_enemyStats.isChanged)
        {
            animator.SetBool("isWalking", false);
            return newIdleState;
        }

        if (needsHidingSpot)
        {
            FindNewHidingSpot();
        }

        float distanceToPlayer = Vector3.Distance(_target.position, transform.position);


        if (distanceToPlayer > backToIdleDistance)
        {
            animator.SetBool("isWalking", false);
            _enemyStats.damaged = false;
            idleState.isWandering = true;
            return idleState;
        }

        if (distanceToPlayer < 15f)
        {
            float lookRange = 40f;
            RaycastHit hitInfo;
            if (Physics.Raycast(lookPoint.transform.position, (_target.position - transform.position), out hitInfo, lookRange))
            {
                if (hitInfo.transform.gameObject.CompareTag("Player") && _scaredAttack)
                {
                    _scaredAttack = false;
                    reloadTimer = 4f;
                    isInAttackMode = true;
                }
                else
                {
                    isInAttackMode = false;
                }
            }
        }
        if (!_scaredAttack)
        {
            reloadTimer -= Time.deltaTime;
            if(reloadTimer <= 0 && reloadTimer > -1f)
            {
                _scaredAttack = true;
            }            
        }

        if (isHiding)
        {
            GoHide();
        }

        if (isInAttackMode)
        {
            reloadTimer = 4f;
            isHiding = false;
            needsHidingSpot = true;
            animator.SetBool("isWalking", false);
            isInAttackMode = false;
            _agent.SetDestination(transform.position);
            return attackState;
        }
        else
        {
            return this;
        }
    }

    void GoHide()
    {
        _agent.SetDestination(hidingSpot.transform.position);
        animator.SetBool("isWalking", true);
        float distance = Vector3.Distance(hidingSpot.transform.position, transform.position);
        if (distance < 4f)
        {
            System.Array.Clear(hidingSpots, 0, hidingSpots.Length);            
            isInAttackMode = true;
        }
    }

    void FindNewHidingSpot()
    {        
        isHiding = false;
        hidingSpots = GameObject.FindGameObjectsWithTag("HidingSpot");
        int index = Random.Range(0, hidingSpots.Length);

        hidingSpot = hidingSpots[index];
        Debug.Log(hidingSpot.name);
        float distance = Vector3.Distance(hidingSpot.transform.position, transform.position);
        if (distance < 10f)
        {
            return;           
        }
        else if (distance > maxHidingSpotSearchRange)
        {
            return;
        }
        else
        {
            needsHidingSpot = false;
            isHiding = true;
        }
    }

}
