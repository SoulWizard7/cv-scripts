﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTakeDamageFromTrigger : MonoBehaviour
{
    PlayerHealth _playerHealth;
    public int damage;
    [Space(10)]

    [Header("Use Timer /// DPS")]
    public bool useTimer;
    float timer;

    private void Start()
    {
        timer = 1;
        _playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (useTimer)
        {
            if (other.CompareTag("Player"))
            {
                timer -= Time.deltaTime;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !useTimer)
        {
            Debug.Log(gameObject.name + " hit player");
            _playerHealth.PlayerTakeDamage(damage);
        }
    }

    void Update()
    {

        if (timer <= 0 & useTimer)
        {
            Debug.Log(gameObject.name + " hit player");
            _playerHealth.PlayerTakeDamage(damage);
            timer = 1;
        }
    }
}
