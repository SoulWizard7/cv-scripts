﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesPerLevel : MonoBehaviour
{
    public int enemyMaxValue;

    public int enemiesValue;

    public List<GameObject> list;

    float increase;

    float changePercent;

    LevelPostProcessColor levelPostProcessColor;

    void Start()
    {
        EnemiesCount();
        levelPostProcessColor = gameObject.GetComponent<LevelPostProcessColor>();
    }    
        
    public void IncreaceValue(int _value)
    {
        enemiesValue += _value;

        CountNewValue();
        levelPostProcessColor.ChangePostProcessColor(changePercent);
    }

    public void CountNewValue()
    {
        
        increase = enemyMaxValue - enemiesValue;

        increase = increase / enemyMaxValue;

        changePercent = 1 - increase;

        //Debug.Log(increase);
        //Debug.Log(changePercent + " percent");
    }

    void EnemiesCount()
    {        
        foreach (EnemyStats enemy in transform.GetComponentsInChildren<EnemyStats>())
        {            
            list.Add(enemy.gameObject);

            enemyMaxValue += enemy.enemyValue;
        }
    }
}
