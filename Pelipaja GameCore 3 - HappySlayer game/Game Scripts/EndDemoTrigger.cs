﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndDemoTrigger : MonoBehaviour
{
    [SerializeField] GameObject endText;

    
    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(End());
    }

    IEnumerator End()
    {
        endText.SetActive(true);
        yield return new WaitForSeconds(8f);
        Cursor.visible = true;
        endText.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
