﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    EnemiesPerLevel epl;

    public float health = 50f;

    public bool damaged = false;

    public bool isChanged = false;

    public int enemyValue;

    EnemyAudioclips audioclips;
    EnemyIdleAudio enemyIdleAudio;

    bool isDead;


    float lastHitTime;


    private void Start()
    {
        epl = gameObject.GetComponentInParent<EnemiesPerLevel>();
        audioclips = GetComponentInChildren<EnemyAudioclips>();
        enemyIdleAudio = GetComponentInChildren<EnemyIdleAudio>();
    }

    public void TakeDamage (float amount)
    {
        if (isDead) return;

        // PLAY HURT AUDIO
        if (Time.time > lastHitTime + .3f)
        {
            audioclips.PlayTakeHitClip();
        }
        lastHitTime = Time.time;

        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
        damaged = true;
    }

    void Die()
    {
        isDead = true;

        // STATE FOR ENEMY HAPPY IDLE
        enemyIdleAudio.SetAngryState(false);

        Debug.Log("kissa");
        //Destroy(gameObject);
        epl.IncreaceValue(enemyValue);
        isChanged = true;

        // PLAY DIE AUDIO
        audioclips.PlayWuhuuClip();
    }
}
