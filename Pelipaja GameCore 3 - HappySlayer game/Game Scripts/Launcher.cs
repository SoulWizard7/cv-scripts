﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    [SerializeField] float force;
    
    PlayerMovement playerMovement;
    float originalJumpHeight;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerMovement = other.GetComponent<PlayerMovement>();
            originalJumpHeight = playerMovement.jumpHeight;
            playerMovement.jumpHeight = force;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerMovement.jumpHeight = originalJumpHeight;
        }
    }
}
