﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SproutGrass : MonoBehaviour
{
    float time = 2f;
    float random;

    private void Start()
    {
        random = Random.Range(0f, 0.7f);
        var scaleTo = new Vector3((1f + random), (1f + random), (1f + random));

        StartCoroutine(ScaleOverSeconds(gameObject, scaleTo, time));
    }

    public IEnumerator ScaleOverSeconds(GameObject objectToScale, Vector3 scaleTo, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingScale = objectToScale.transform.localScale / 100;
        while (elapsedTime < seconds)
        {
            objectToScale.transform.localScale = Vector3.Lerp(startingScale, scaleTo, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }        
    }




    /*
    [SerializeField]
    private AnimationCurve lerpCurve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));

    [SerializeField]
    private Vector2 moveAreaXY = new Vector2(5f, 5f);

    [SerializeField]
    private float moveTime = 1f, waitTime = 0.5f;

    bool isBig = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MoveRandomly());
    }

    private IEnumerator MoveRandomly()
    {
        while (true && !isBig)
        {
            float timer = 2f;
            Vector3 moveFrom = transform.localScale;
            Vector3 moveTo = transform.localScale * 2;
            while (timer <= moveTime)
            {
                //timer += Time.deltaTime;
                float lerp01 = timer / moveTime;
                float curveValue = lerpCurve.Evaluate(lerp01);
                transform.localScale = Vector3.LerpUnclamped(moveFrom, moveTo, curveValue);

                yield return null;
            }

            yield return isBig = true;
            //yield return new WaitForSeconds(waitTime);
        }
        yield return null;
    }*/
}
