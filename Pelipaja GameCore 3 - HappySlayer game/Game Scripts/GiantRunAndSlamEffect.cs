﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantRunAndSlamEffect : MonoBehaviour
{
    ImpactReceiver ir;
    public float force;
    public PlayerHealth _playerHealth;
    public int damage;

    private void Start()
    {
        _playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ir = other.gameObject.GetComponent<ImpactReceiver>();
            Vector3 dir = other.transform.position - transform.position;

            ir.AddImpact(dir, force);

            _playerHealth.PlayerTakeDamage(damage);
        }
    }
}
