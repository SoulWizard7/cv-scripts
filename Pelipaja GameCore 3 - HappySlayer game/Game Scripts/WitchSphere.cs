﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WitchSphere : MonoBehaviour
{
    float time = 2.5f;
    float random;
    float timer;
    float time2 = 1f;

    

    private void Start()
    {
        
        timer = 5f;
        random = Random.Range(0f, 0.7f);
        var scaleTo = new Vector3(1f, 1f, 1f);

        StartCoroutine(ScaleOverSeconds(gameObject, scaleTo, time));
    }
    

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        Destroy(gameObject, 15f);
        if (timer <= 0f)
        {
            var scaleDown = new Vector3(0f, 0f, 0f);
            StartCoroutine(ScaleDown(gameObject, scaleDown, time2));
        }
    }

    public IEnumerator ScaleOverSeconds(GameObject objectToScale, Vector3 scaleTo, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingScale = objectToScale.transform.localScale / 100;
        while (elapsedTime < seconds)
        {
            objectToScale.transform.localScale = Vector3.Lerp(startingScale, scaleTo, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator ScaleDown(GameObject objectToScale, Vector3 scaleDown, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingScale = objectToScale.transform.localScale;
        while (elapsedTime < seconds)
        {
            objectToScale.transform.localScale = Vector3.Lerp(startingScale, scaleDown, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
