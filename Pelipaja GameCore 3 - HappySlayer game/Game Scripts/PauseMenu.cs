﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] GameObject pausePanel;
    [SerializeField] AudioMixerSnapshot pased;
    [SerializeField] AudioMixerSnapshot unpased;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pausePanel.SetActive(!pausePanel.activeSelf);
            PauseGame();
        }
    }

    private void PauseGame()
    {
        Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        Lowpass();
    }

    private void Lowpass()
    {
        if (Time.timeScale == 0)
            pased.TransitionTo(0.01f);
        else
            unpased.TransitionTo(0.01f);
    }
}

