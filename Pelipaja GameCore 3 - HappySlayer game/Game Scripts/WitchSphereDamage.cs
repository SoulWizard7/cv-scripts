﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WitchSphereDamage : MonoBehaviour
{
    PlayerHealth _playerHealth;
    public int damage;
    [Space(10)]

    [Header("Use Timer /// DPS")]
    public bool useTimer;
    float timer;

    private void Start()
    {
        timer = 1;
        _playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (useTimer)
        {
            if (other.CompareTag("Player"))
            {
                timer -= Time.deltaTime;
            }
        }
        else
        {
            _playerHealth.PlayerTakeDamage(damage);
        }


        //Debug.Log("Witch Sphere hit " + other.gameObject.name);
    }

    void Update()
    {
        if(timer <= 0)
        {
            _playerHealth.PlayerTakeDamage(damage);
            timer = 1;
        }
    }
}
