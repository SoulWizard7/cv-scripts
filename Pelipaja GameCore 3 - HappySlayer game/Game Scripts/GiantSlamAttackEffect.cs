﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantSlamAttackEffect : MonoBehaviour
{
    [SerializeField] float force;

    PlayerMovement playerMovement;
    CapsuleCollider col;
    float originalJumpHeight;
    float _timer = 0.2f;

    PlayerHealth _playerHealth;
    public int damage;

    private void Start()
    {
        _playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
    }

    private void Update()
    {
        _timer -= Time.deltaTime;
        if(_timer <= 0)
        {
            col = gameObject.GetComponent<CapsuleCollider>();
            col.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerMovement = other.GetComponent<PlayerMovement>();
            originalJumpHeight = playerMovement.jumpHeight;
            playerMovement.jumpHeight = force;
            playerMovement.Jump();
            col = gameObject.GetComponent<CapsuleCollider>();
            col.enabled = false;

            _playerHealth.PlayerTakeDamage(damage);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerMovement.jumpHeight = originalJumpHeight;
        }
    }
}
