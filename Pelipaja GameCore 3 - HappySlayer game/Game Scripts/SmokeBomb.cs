﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeBomb : MonoBehaviour
{
    SphereCollider _collider;
    ParticleSystem _ps;
    Rigidbody _rb;

    void Start()
    {
        _collider = GetComponent<SphereCollider>();
        _ps = GetComponentInChildren<ParticleSystem>();
        _rb = GetComponent<Rigidbody>();
    }


    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == ("Goblin"))
        {
            Physics.IgnoreCollision(collision.collider, _collider);
        }
        else
        {
            _rb.velocity = Vector3.zero;
            _rb.angularVelocity = Vector3.zero;
            gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            _ps.Play();
            Destroy(gameObject, 10f);
        }
    }
}
