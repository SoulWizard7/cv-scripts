﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PauseMenuVer2 : MonoBehaviour
{
    [SerializeField] GameObject pausePanel;
    [SerializeField] AudioMixerSnapshot pased;
    [SerializeField] AudioMixerSnapshot unpased;

    public bool _gameIsPaused;
    [SerializeField] GameObject pauseMenuUI;
    [SerializeField] GameObject playerWeapons;

    [Space(10)]
    [Header("MouseLook")]
    [SerializeField] MouseLook mouseLook;
    [SerializeField] Slider sliderMouseLook;
    [SerializeField] TMP_Text textMouseLook;
    [SerializeField] float mouseLookValue;

    [Space(10)]
    [Header("SFX & Music Volume Coming")]
    [SerializeField] GameObject volumeStuff;
    // Start is called before the first frame update
    void Start()
    {
        sliderMouseLook.value = mouseLook.mouseSensitivity;
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pausePanel.SetActive(!pausePanel.activeSelf);
            PauseGame();
        }
        if (_gameIsPaused)
        {
            mouseLookValue = mouseLook.mouseSensitivity;
            textMouseLook.text = mouseLookValue.ToString();
        }
    }
    //Resumes the paused game
    public void Resume()
    {
        //Debug.Log("Resume button is working");
        Time.timeScale = 1;
        playerWeapons.SetActive(true);
        pauseMenuUI.SetActive(false);
        _gameIsPaused = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
    }

    //Pauses the game
    void Pause()
    {
        Time.timeScale = 0;
        playerWeapons.SetActive(false);
        pauseMenuUI.SetActive(true);
        _gameIsPaused = true;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }
    public void BackToMainMenu()
    {
        Time.timeScale = 1;
        Cursor.visible = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    private void PauseGame()
    {
        Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        Lowpass();
    }

    private void Lowpass()
    {
        if (Time.timeScale == 0)
        {
            pased.TransitionTo(0.01f);
            _gameIsPaused = true;
            playerWeapons.SetActive(false);
        }
        else
        {
            unpased.TransitionTo(0.01f);
            _gameIsPaused = false;
            playerWeapons.SetActive(true);
        }
    }
}