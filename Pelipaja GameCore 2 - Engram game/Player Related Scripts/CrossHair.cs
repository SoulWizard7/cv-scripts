﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHair : MonoBehaviour
{


    
    [SerializeField] private Transform parentObject;
    Vector3 targetDir;
    Camera _cam;

    /*void Awake()
    {
        Cursor.visible = false;
    }*/

    private void Start()
    {
        parentObject = transform.parent;
        _cam = Camera.main; // store a ref to the camera at start Camera.Main is expensive performance wise
    }

    private void Update()
    {        
        targetDir =
        _cam.ScreenToWorldPoint(Input.mousePosition) - parentObject.position;


        // We can use a vector2 to determine the angle between the pointing forward
        // of the parent and the target direction
        float a = Vector2.Angle(targetDir, parentObject.up);

        float angle = (Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg) - 90f;

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }
}
