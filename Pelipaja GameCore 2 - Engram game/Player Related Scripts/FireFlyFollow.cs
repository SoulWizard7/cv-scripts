﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFlyFollow : MonoBehaviour
{
    //[SerializeField]
    //private Transform target;

    public Transform target;
    public Transform projectileTarget;

    public GameObject player;    
    PlayerMovement pm;

    public Transform playerPosition;

    [Range(0f, 1f)]
    [SerializeField]
    private float lerpSpeed = 0.1f;

    [SerializeField]
    private float followDistance = 1f;

    public void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        pm = player.GetComponent<PlayerMovement>();

        playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
        transform.position = playerPosition.position;        
    }

    void Update()
    {
        if (pm.ropeExists)
        {
            projectileTarget = GameObject.FindGameObjectWithTag("Projectile").transform;
        }
        else
        {
            projectileTarget = playerPosition;
        }
    }

    void FixedUpdate()
    {
        if (pm.ropeExists)
        {
            Vector3 fromTargetToTransform = transform.position - projectileTarget.position;
            if (fromTargetToTransform.magnitude > followDistance)
            {
                Vector3 distanceFromTarget = fromTargetToTransform.normalized * followDistance;

                Vector3 moveToWorldPosition = projectileTarget.position + distanceFromTarget;

                transform.position = Vector3.Lerp(transform.position, moveToWorldPosition, lerpSpeed);
            }
        }
        else
        {
            Vector3 fromTargetToTransform = transform.position - target.position;
            if (fromTargetToTransform.magnitude > followDistance)
            {
                Vector3 distanceFromTarget = fromTargetToTransform.normalized * followDistance;

                Vector3 moveToWorldPosition = target.position + distanceFromTarget;

                transform.position = Vector3.Lerp(transform.position, moveToWorldPosition, lerpSpeed);
            }
        }
    }
}
