﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{

    public int level;
    public int health;
    public bool candleLight;
    public float[] position; // position uses 3 save floats

    public bool collectable1;
    public bool collectable2;
    public bool collectable3;
    public bool collectable4;
    public bool collectable5;
    public bool collectable6;
    public bool collectable7;
    public bool collectable8;
    public bool collectable9;

    public PlayerData (GameManager player)
    {
        level = player.level;
        health = player.health;
        candleLight = player.candleLight;

        collectable1 = player.collectable1;
        collectable2 = player.collectable2;
        collectable3 = player.collectable3;
        collectable4 = player.collectable4;
        collectable5 = player.collectable5;
        collectable6 = player.collectable6;
        collectable7 = player.collectable7;
        collectable8 = player.collectable8;
        collectable9 = player.collectable9;

        position = new float[3];  // position save floats
        position[0] = player.transform.position.x;
        position[1] = player.transform.position.y;
        position[2] = player.transform.position.z;
    }

}
