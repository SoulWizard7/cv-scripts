﻿using System.Collections;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Restart : MonoBehaviour
{
    GameObject player;
    Rigidbody2D playerRB;

    GameManager gm;
    PlayerMovement pm;

    GameObject fireFly1;
    GameObject fireFly2;
    GameObject fireFly3;
    FireFlyFollow fff1;
    FireFlyFollow fff2;
    FireFlyFollow fff3;

    GameObject projectile;

    public GameObject blackOutSquare;
    public bool isFadedBlack;
    public float fadeTime;
    Image blackScreen;

    GameObject boulderSpawner;
    BoulderSpawner bs;
    GameObject boulder;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");        
        playerRB = player.GetComponent<Rigidbody2D>();
        gm = player.GetComponent<GameManager>();
        pm = player.GetComponent<PlayerMovement>();
        boulderSpawner = GameObject.Find("BoulderSpawner");
        bs = boulderSpawner.GetComponent<BoulderSpawner>();

        fireFly1 = GameObject.Find("Firefly 1");
        fireFly2 = GameObject.Find("Firefly 2");
        fireFly3 = GameObject.Find("Firefly 3");

        fff1 = fireFly1.GetComponent<FireFlyFollow>();
        fff2 = fireFly2.GetComponent<FireFlyFollow>();
        fff3 = fireFly3.GetComponent<FireFlyFollow>();

        blackScreen = blackOutSquare.GetComponent<Image>();

        StartCoroutine(FadeToClear());
    }

    void Update()
    {
        if (pm.ropeExists)
        {
            projectile = GameObject.FindGameObjectWithTag("Projectile");
        }
        /*if (Input.GetKeyDown(KeyCode.R))          Restart game and Load Game shortcuts for game testing -- REMOVED
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadLastCheckPoint();
        }*/
    }

    public void LoadLastCheckPoint()
    {
        StartCoroutine(FadeToBlack(() => Load()));        
    }

    public void Load()
    {        
        gm.LoadPlayer();
        
        if (pm.ropeExists)
        {
            DestroyProjectile();
        }
        playerRB.velocity = Vector2.zero;
        playerRB.angularVelocity = 0f;
        pm.movementSpeed = 3f;
        
        if (bs.boulderExists)
        {
            boulder = GameObject.Find("Boulder(Clone)");
            Destroy(boulder);
            bs.boulderExists = false;
            bs.bc2D.enabled = true;
        }
        
        StartCoroutine(FadeToClear());
        
        AudioManager.instance.StopSFX();
        AudioManager.instance.StopVoice();
        pm.animator.SetBool("isDead", false);
        FireFlyReload();
    }

    void FireFlyReload()        //fireflys restart att player location when game load
    {
        fff1.Awake();
        fff2.Awake();
        fff3.Awake();
    }
    void DestroyProjectile()
    {
        pm.ropeExists = false;
        Destroy(projectile);
    }
 
    public IEnumerator FadeToClear()
    {
        blackOutSquare.gameObject.SetActive(true);
        blackScreen.color = Color.black;

        float rate = 1.0f / fadeTime;

        float progress = 0.0f;

        while (progress < 1.0f)
        {
            blackScreen.color = Color.Lerp(Color.black, Color.clear, progress);

            progress += rate * Time.deltaTime;

            yield return null;
        }

        blackScreen.color = Color.clear;
        blackOutSquare.gameObject.SetActive(false);
        
    }
    public IEnumerator FadeToBlack(Action loadMethod)
    {
        blackOutSquare.gameObject.SetActive(true);
        blackScreen.color = Color.clear;

        float rate = 1.0f / fadeTime;

        float progress = 0.0f;

        while (progress < 1.0f)
        {
            blackScreen.color = Color.Lerp(Color.clear, Color.black, progress);

            progress += rate * Time.deltaTime;

            yield return null;
        }

        blackScreen.color = Color.black;
        blackOutSquare.gameObject.SetActive(false);

        loadMethod();

        
    }
}
