﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb;
    public Animator animator;
    GameManager gm;
    Restart restart;
    private bool m_FacingRight = true;

    /*
     Mietin, että olisiko mahdollista laittaa headit sen mukaan, mitä asia koskee? Esim. movement, jump, hook ja candle/other?
     Myös, voitais yhdessä vaikka pohtia yleisesti, että tarviiko boolien olla public? Eihän niillä ole mitään tarvetta olla public, jos se toimii. Boolit yleensä pidetään mieluummin privatena ettei niitä mennä sörkkimään.
     */

    [Header("Movement")]
    public bool canMove = true;
    public float movementSpeed = 3;
    public float jumpForce;
    public float projectileLaunchSpeed = 15f;
    public bool isFlying = false;

    [SerializeField] private LayerMask isGroundedLayerMask;
    
    private bool right;
    private bool left;
    private bool up;
    private bool down;
    private bool up_isAxisInUse = false;
    private bool down_isAxisInUse = false;    

    [Header("Candle")]
    public bool candleLight = true;
    public GameObject cLight;
    public float fallFlickerIntensity;
    public float fallTime;
    public float lightStabilizeTime;
    public float lightIntensityStabilizeTime;
    SoftFlicker fallFlicker;

    [Header("Hook")]
    public bool attached = false;
    public bool ropeExists = false;    
    public float flyForce = 0.2f;
    public float pushForce = 2f;
    public GameObject hookPrefab;
    public GameObject hookPosition;
    public Transform attachedTo;
    public GameObject disregard;
    public int shootHookSFX;    

    [Header("GroundChecking")]
    public bool isGrounded = false;
    public GameObject groundChecker;    //groundchecker and boxcollider are now childobjects, interfered with triggers(trigged 2 times)
    public BoxCollider2D box2D;

    private HingeJoint2D hj;
    private Transform firePoint;
    
    [Header("SFX")]
    public int screamPlay;

    void Start()
    {
        gm = GetComponent<GameManager>();
        rb = GetComponent<Rigidbody2D>();
        hj = GetComponent<HingeJoint2D>();
        restart = GetComponent<Restart>();
        animator = GetComponent<Animator>();
        firePoint = GameObject.Find("FirePoint").transform;
        box2D = groundChecker.GetComponent<BoxCollider2D>();
        cLight = GameObject.Find("CandleLight");        
        fallFlicker = cLight.GetComponent<SoftFlicker>();

        animator.SetBool("isDead", false);

        gm.StartMenu();
    }
    void Update()
    {
        //isGrounded code        
        float extraHeightGrounded = 0.65f;
        isGrounded = Physics2D.BoxCast(box2D.bounds.center, box2D.bounds.size, 0f, Vector2.down, extraHeightGrounded, isGroundedLayerMask);
        
        if (isGrounded)
        {
            canMove = true;
            Jump();            
            isFlying = false;

            animator.SetBool("isJumping", false);

            fallTime = 0;

            if (fallFlicker.lerpSpeed > 4)
            {
                lightStabilizeTime += Time.deltaTime;
                fallFlicker.lerpSpeed = (fallFlicker.lerpSpeed) - lightStabilizeTime;
            }
            if (fallFlicker.lerpSpeed < 4)
            {
                lightStabilizeTime = 0;
                //fallFlicker.lerpSpeed = 4;
            }
            if (fallFlicker.maxIntensity < 1)
            {
                lightIntensityStabilizeTime += Time.deltaTime;
                fallFlicker.maxIntensity = 0.5f + lightIntensityStabilizeTime;
            }

            if (fallFlicker.maxIntensity > 1)
            {
                fallFlicker.maxIntensity = 1f;
                lightIntensityStabilizeTime = 0;
            }
        }
        
        hookPosition = GameObject.FindWithTag("Hook");

        if (!ropeExists && Input.GetButtonDown("Fire1") && !gm.GameIsPaused)
        {
            ShootHook();
            ropeExists = true;
            AudioManager.instance.PlaySFX(shootHookSFX);
        }

        if (candleLight)
        {
            cLight.SetActive(true);            
        }
        if (!candleLight)
        {
            cLight.SetActive(false);            
        }

        if (!isGrounded && !attached)    // timer & code for fall flicker and light extinguish
        {
            fallTime += Time.deltaTime;
            
            fallFlicker.lerpSpeed = 4 + fallTime;
            
            fallFlicker.maxIntensity = 1 / (1f + fallTime);

            animator.SetBool("isJumping", true);
            animator.SetBool("isSwinging", false);
            
            if (fallTime > 2.8 && fallTime < 2.9)
            {
                AudioManager.instance.PlaySFX(screamPlay);
            }
        }
    }

    void LateUpdate()
    {
        if (attached)
        {
            fallFlicker.lerpSpeed = 4;
            fallFlicker.maxIntensity = 1f;
            fallTime = 0;

            isGrounded = false;
            canMove = false;
            Slider();
            if (Input.GetKeyDown(KeyCode.Space))
            {                
                Detach();                
            }
            animator.SetBool("isJumping", false);
            animator.SetBool("isSwinging", true);
        }
    }

    // Testing and moved "Move()" into fixedUpdate, physics not working correctly in build

    void FixedUpdate()
    {
        Move();
    }

    // Attach, Detach, Slider, Slide all is from juul1a youtube channel rope tutorial. watch vids for more info.

    public void Attach(Rigidbody2D ropeBone)
    {
        ropeBone.gameObject.GetComponent<RopeSegment>().isPlayerAttached = true;
        hj.connectedBody = ropeBone;
        hj.enabled = true;
        attachedTo = ropeBone.gameObject.transform.parent;
        attached = true;
        isGrounded = false;
    }
    void Detach()
    {
        hj.connectedBody.gameObject.GetComponent<RopeSegment>().isPlayerAttached = false;
        attached = false;
        hj.enabled = false;
        hj.connectedBody = null;
        isFlying = true;
        StartCoroutine(WaitUntilCanAttachRopeAgain());         //player attaches directly to rope, this coroutine waits 1s before rope can be attached again
        animator.SetBool("isSwinging", false);
    }

    private IEnumerator WaitUntilCanAttachRopeAgain()
    {                
        yield return new WaitForSeconds(1f);        
        yield return attachedTo = null;        
    }

    void ShootHook()
    {
        GameObject hook = Instantiate(hookPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D hookrb = hook.GetComponent<Rigidbody2D>();
        hookrb.AddForce(firePoint.up * projectileLaunchSpeed, ForceMode2D.Impulse);
    }

    public void Move()
    {
        // controller support is done but not tested
        //pushforce is used when attached to rope, flyforce is used minimal movement while detaching and flying off rope.
        
        float x = Input.GetAxisRaw("Horizontal");

        //horizontalMove = Input.GetAxisRaw("Horizontal") * speed;

        animator.SetFloat("Speed", Mathf.Abs(x));        

        if (canMove)
        {
            right = Input.GetAxisRaw("Horizontal") > 0;
            left = Input.GetAxisRaw("Horizontal") < 0;
        }

        if (canMove)
        {
            float moveBy = x * movementSpeed;
            rb.velocity = new Vector2(moveBy, rb.velocity.y);
        }

        if (!right && !left && !attached && canMove)
        {
            float moveRight = 0;
            rb.velocity = new Vector2(moveRight, rb.velocity.y);            
        }

        // If the input is moving the player right and the player is facing left...
        if (right && canMove && !m_FacingRight)
        {
            float moveRight = 1 * movementSpeed;
            // ... flip the player.
            Flip();
            rb.velocity = new Vector2(moveRight, rb.velocity.y);            
        }

        // Otherwise if the input is moving the player left and the player is facing right...
        if (left && canMove && m_FacingRight)
        {
            float moveLeft = -1 * movementSpeed;
            // ... flip the player.
            Flip();
            rb.velocity = new Vector2(moveLeft, rb.velocity.y);            
        }

        if /*(!canMove)*/ (attached)
        {
            rb.AddRelativeForce(new Vector3(x, 0, 0) * pushForce);
        }
        if (isFlying)
        {
            rb.AddRelativeForce(new Vector3(x, 0, 0) * flyForce);
        }
    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !Input.GetKey(KeyCode.S))
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }
    }
    void Slider()
    {
        //extra (axisInUse) code is for controller support, moves up and down rope one step per analogstick push up or down

        up = Mathf.Round(Input.GetAxisRaw("Vertical")) > 0;
        down = Mathf.Round(Input.GetAxisRaw("Vertical")) < 0;

        if (up)
        {
            if (up_isAxisInUse == false)
            {                
                Slide(1);
                up_isAxisInUse = true;
            }
        }
        if (down)
        {
            if (down_isAxisInUse == false)
            {                
                Slide(-1);
                down_isAxisInUse = true;
            }
        }
        if (Mathf.Round(Input.GetAxisRaw("Vertical")) == 0)
        {
            up_isAxisInUse = false;
            down_isAxisInUse = false;
        }
    }
    public void Slide(int direction)
    {
        RopeSegment myConnection = hj.connectedBody.gameObject.GetComponent<RopeSegment>();
        GameObject newSeg = null;
        if (direction > 0)
        {
            if (myConnection.connectedAbove != null)
            {
                if (myConnection.connectedAbove.gameObject.GetComponent<RopeSegment>() != null)
                {
                    newSeg = myConnection.connectedAbove;
                }
            }
        }
        else
        {
            if (myConnection.connectedBelow != null)
            {
                newSeg = myConnection.connectedBelow;
            }
        }
        if (newSeg != null)
        {
            transform.position = newSeg.transform.position;
            myConnection.isPlayerAttached = false;
            newSeg.GetComponent<RopeSegment>().isPlayerAttached = true;
            hj.connectedBody = newSeg.GetComponent<Rigidbody2D>();
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (!attached)
        {
            if (col.gameObject.CompareTag("Rope"))
            {
                if (attachedTo != col.gameObject.transform.parent)
                {
                    if (disregard == null || col.gameObject.transform.parent.gameObject != disregard)
                    {
                        Attach(col.gameObject.GetComponent<Rigidbody2D>());
                    }
                }
            }
        }
        if (col.gameObject.CompareTag("Spike"))
        {
            Die();
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Spike"))
        {
            Die();
        }
    }
    public void Die()
    {
        animator.SetBool("isDead", true);
        movementSpeed = 0f;
        restart.LoadLastCheckPoint();
                
        canMove = false;
        attached = false;
    }

    //Player Turn Animation by Brackeys https://github.com/Brackeys/2D-Character-Controller/blob/master/CharacterController2D.cs
    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}