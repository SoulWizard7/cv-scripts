﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Canvas Menus")]
    [SerializeField] private GameObject pauseMenuUI;
    [SerializeField] private GameObject startMenuUI;
    [SerializeField] private GameObject endMenuUI;
    [SerializeField] private GameObject endMenuUI2;
    public bool GameIsPaused = false;


    [Header("SaveData")]
    public int level = 3;
    public int health = 40;
    public bool candleLight = true;

    public bool collectable1 = false;
    public bool collectable2 = false;
    public bool collectable3 = false;
    public bool collectable4 = false;
    public bool collectable5 = false;
    public bool collectable6 = false;
    public bool collectable7 = false;
    public bool collectable8 = false;
    public bool collectable9 = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
                Cursor.visible = false;
            }
            else
            {
                Pause();
            }
        }        
    }

    #region PauseMenu
    //Resumes the paused game
    public void Resume()
    {
        Debug.Log("Resume button is working");
        Time.timeScale = 1;
        pauseMenuUI.SetActive(false);
        startMenuUI.SetActive(false);
        GameIsPaused = false;
        Cursor.visible = false;
    }

    //Pauses the game
    void Pause()
    {
        Time.timeScale = 0;
        pauseMenuUI.SetActive(true);

        GameIsPaused = true;
        Cursor.visible = true;
    }

    //Game Start Screen

    public void StartMenu()
    {
        Time.timeScale = 0;
        startMenuUI.SetActive(true);

        GameIsPaused = true;
        Cursor.visible = true;
    }

    public void EndMenu()
    {
        Time.timeScale = 0;
        endMenuUI.SetActive(true);

        GameIsPaused = true;
        Cursor.visible = true;

    }

    public void EndMenu2()
    {
        Time.timeScale = 0;
        endMenuUI2.SetActive(true);

        GameIsPaused = true;
        Cursor.visible = true;

    }
    public void Next()
    {
        endMenuUI.SetActive(false);
        EndMenu2();
        Cursor.visible = true;
    }

    public void BackToMainMenu()
    {
        Time.timeScale = 1;
        Cursor.visible = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    #endregion

    #region Save&Load data
    // save data & new collectables must be added here and beneath, and in playerdata script, and CollectableChecker script
    public void SavePlayer()
    {
        SaveSystem.SavePlayer(this);
    }

    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();

        level = data.level;
        health = data.health;
        candleLight = data.candleLight;

        collectable1 = data.collectable1;
        collectable2 = data.collectable2;
        collectable3 = data.collectable3;
        collectable4 = data.collectable4;
        collectable5 = data.collectable5;
        collectable6 = data.collectable6;
        collectable7 = data.collectable7;
        collectable8 = data.collectable8;
        collectable9 = data.collectable9;


        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        transform.position = position;

    }
    #endregion



}
