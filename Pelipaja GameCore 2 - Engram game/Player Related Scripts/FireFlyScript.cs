﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFlyScript : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve lerpCurve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));

    [SerializeField]
    private Vector2 moveAreaXY = new Vector2(5f, 5f);

    [SerializeField]
    private float moveTime = 1f, waitTime = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MoveRandomly());
    }

    private IEnumerator MoveRandomly()
    {
        while (true)
        {
            float timer = 0f;
            Vector3 moveFrom = transform.localPosition;
            Vector3 moveTo = RandomPosition();
            while (timer <= moveTime)
            {
                timer += Time.deltaTime;
                float lerp01 = timer / moveTime;
                float curveValue = lerpCurve.Evaluate(lerp01);
                transform.localPosition = Vector3.LerpUnclamped(moveFrom, moveTo, curveValue);
                yield return null;
            }
            yield return new WaitForSeconds(waitTime);
        }
    }

    private Vector3 RandomPosition()
    {
        float x = Random.Range(-moveAreaXY.x, moveAreaXY.x);
        float y = Random.Range(-moveAreaXY.y, moveAreaXY.y);
        float z = transform.position.z;
        return new Vector3(x, y, z);
    }
}
