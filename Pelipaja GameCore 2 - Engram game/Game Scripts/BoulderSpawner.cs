﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderSpawner : MonoBehaviour
{
    public GameObject boulderPrefab;
    public bool boulderExists;
    public int sfxToPlay;
    public BoxCollider2D bc2D;

    void Start()
    {
        bc2D = GetComponent<BoxCollider2D>();
        boulderExists = false;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && !boulderExists)
        {
            Instantiate(boulderPrefab);
            boulderExists = true;
            StartCoroutine(Delay());
            bc2D.enabled = false;
        }
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(2);
        AudioManager.instance.PlayVoice(sfxToPlay);
    }
}
