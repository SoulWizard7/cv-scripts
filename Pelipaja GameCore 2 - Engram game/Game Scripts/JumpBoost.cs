﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBoost : MonoBehaviour
{
    Rigidbody2D rb;
    GameObject player;
    public float boostForce = 15f;
    public Animator animator;
    PlayerMovement pm;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb = player.GetComponent<Rigidbody2D>();
        pm = player.GetComponent<PlayerMovement>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == ("Player"))
        {
            rb.velocity = new Vector2(rb.velocity.x, boostForce);
            animator.SetTrigger("jump");
            pm.fallTime = -2;
        }            
    }
}
