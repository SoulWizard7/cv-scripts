﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicToPlay : MonoBehaviour
{
    public static MusicToPlay instance;

    public int musicToPlay;
    public bool musicStarted;

    void Awake()
    {
        if (instance != null)
        {
            GameObject.Destroy(instance);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }
    void LateUpdate()
    {
        if (!musicStarted)
        {
            musicStarted = true;
            AudioManager.instance.PlayBGM(musicToPlay);
        }
    }
}
