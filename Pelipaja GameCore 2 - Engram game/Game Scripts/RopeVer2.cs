﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeVer2 : MonoBehaviour
{
    public Rigidbody2D hook;
    public GameObject[] prefabRopeSegs;
    public int numLinks = 5;
    public GameObject prefabRopeSeg;
    public GameObject lastSeg;
    public GameObject firstSeg;
    public GameObject prefabFirstSeg;
    public Transform projectile;

    void Start()
    {
        projectile = GameObject.FindGameObjectWithTag("Projectile").transform;
        GenerateRope();
    }
    void GenerateRope()
    {
        Rigidbody2D prevBod = hook;

        GameObject firstSeg = Instantiate(prefabFirstSeg/*, projectile.position, projectile.rotation*/);
        firstSeg.transform.parent = transform;
        firstSeg.transform.position = transform.position;
        HingeJoint2D hj2 = firstSeg.GetComponent<HingeJoint2D>();
        hj2.connectedBody = prevBod;

        prevBod = firstSeg.GetComponent<Rigidbody2D>();

        for (int i = 0; i < numLinks; i++)
        {
            
            int index = Random.Range(0, prefabRopeSegs.Length);
            GameObject newSeg = Instantiate(prefabRopeSegs[index]);
            newSeg.transform.parent = transform;
            newSeg.transform.position = transform.position;
            //newSeg.transform.rotation = projectile.rotation;
            HingeJoint2D hj = newSeg.GetComponent<HingeJoint2D>();
            hj.connectedBody = prevBod;

            prevBod = newSeg.GetComponent<Rigidbody2D>();
        }
        /*
        GameObject lastSeg = Instantiate(prefabRopeSeg);
        lastSeg.transform.parent = transform;
        lastSeg.transform.position = transform.position;
        HingeJoint2D hj1 = lastSeg.GetComponent<HingeJoint2D>();
        hj1.connectedBody = prevBod;

        prevBod = lastSeg.GetComponent<Rigidbody2D>();*/

    }
}
