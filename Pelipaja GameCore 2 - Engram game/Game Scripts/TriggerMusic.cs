﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMusic : MonoBehaviour
{
    private BoxCollider2D bc2D;
    public int BGMToPlay;

    void Start()
    {
        bc2D = GetComponent<BoxCollider2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            AudioManager.instance.PlayBGM(BGMToPlay);            
            bc2D.enabled = false;
        }
    }
}
