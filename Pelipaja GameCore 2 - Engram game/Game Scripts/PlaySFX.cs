﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySFX : MonoBehaviour
{
    private BoxCollider2D bc2D;
    public int sfxToPlay;

    void Start()
    {
        bc2D = GetComponent<BoxCollider2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            AudioManager.instance.PlaySFX(sfxToPlay);
            bc2D.enabled = false;
        }
    }
}
