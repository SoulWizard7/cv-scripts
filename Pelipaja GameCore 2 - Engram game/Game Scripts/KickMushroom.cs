﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickMushroom : MonoBehaviour
{   
    public Component[] bc2D;

    public Component[] animators;

    ParticleSystem particleSystem;

    private float boostForce = 3f;

    GameObject player;
    Animator playerAnimator;
    Rigidbody2D playerRB;
    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerAnimator = player.GetComponent<Animator>();
        playerRB = player.GetComponent<Rigidbody2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {            
            playerAnimator.SetBool("isKicking", true);

            playerRB.velocity = new Vector2(playerRB.velocity.x, boostForce);

            StartCoroutine(Delay());            
            
            bc2D = GetComponentsInChildren<BoxCollider2D>();
            foreach (BoxCollider2D boxCollider2D in bc2D) boxCollider2D.enabled = false;
            
            animators = GetComponentsInChildren<Animator>();
            foreach (Animator animator in animators) animator.SetTrigger("kick");

            particleSystem.Play();
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1);
        playerAnimator.SetBool("isKicking", false);
    }
}
