﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Rigidbody2D hookrb;
    public GameObject ropePrefab;

    [SerializeField] private LayerMask canHitLayerMask;

    [SerializeField] private LayerMask returnProjectileLayerMask;

    public float rotationSpeed = 200f;
    public float returnSpeed = 7f;
    public Transform player;
    public GameObject playe;
    public PlayerMovement pm;
    public GameObject rope;
    public float maxProjectileDistance;
    public Transform returnPosition;

    public GameObject currentRope;

    public bool returnProjectile = false;
    public bool projectileIsAttached = false;

    public int returnHookSFX;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playe = GameObject.FindGameObjectWithTag("Player");
        returnPosition = GameObject.FindGameObjectWithTag("ReturnPosition").transform;
        pm = playe.GetComponent<PlayerMovement>();
        hookrb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (returnProjectile)
        {
            rope.SetActive(false);
        }

        if (Vector2.Distance(transform.position, player.position) > maxProjectileDistance && !projectileIsAttached) //palauttaa projectile jos max distance ylityy.
        {
            returnProjectile = true;
        }

        if (Input.GetButtonDown("Fire2")/* && pm.ropeExists*/&& pm.isGrounded && !pm.attached) //oikea hiirinappi palauttaa projectile
        {
            returnProjectile = true;
            //Destroy(currentRope);
            rope.SetActive(false);
            AudioManager.instance.PlaySFX(returnHookSFX);
        }
    }
    void FixedUpdate()
    {        
        if (returnProjectile)  //Palauttaa projectile pelaajalle. ReturnPosition korjaa bugin.
        {
            Vector2 direction = (Vector2)returnPosition.position - hookrb.position;
            direction.Normalize();
            float rotateAmount = Vector3.Cross(direction, transform.up).z; //homing missile koodi brackys videosta
            hookrb.angularVelocity = -rotateAmount * rotationSpeed;
            hookrb.velocity = transform.up * returnSpeed;
        }
    }
    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        Debug.Log("projectile did hit");     

        if (((1 << hitInfo.gameObject.layer) & returnProjectileLayerMask) != 0)
        {
            returnProjectile = true;
        }

        if (((1 << hitInfo.gameObject.layer) & canHitLayerMask) != 0 && !returnProjectile) //layermask mihin projectile voi tarttua.
        {
            // projectile jää kohdalleen
            hookrb.velocity = Vector2.zero;
            hookrb.angularVelocity = 0f;
            
            rope.SetActive(true);
            projectileIsAttached = true;
        }
        
        if (hitInfo.gameObject.CompareTag("Player") && !pm.attached && returnProjectile)
        {
            Destroy(gameObject);
            pm.ropeExists = false;
        }        
    }    
}
