﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayFootstep : MonoBehaviour
{
    // This is a test script for footsteps only at this point
    AudioSource audioSource;
    GameObject footStep;

    public bool play;
    // Start is called before the first frame update
    void Start()
    {
        footStep = GameObject.Find("FootStep");
        audioSource = footStep.GetComponent<AudioSource>();
        play = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (play)
        {
            playSFX();
        }
    }
    void playSFX()
    {

        //AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.Play();
        play = false;
    }
}
