﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpDownFromPlatform : MonoBehaviour
{

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player" && Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.Space))
        {
            Jumper();
            Invoke("Jumper", 0.5f);
        }
    }
    void Jumper()
    {
        gameObject.GetComponent<BoxCollider2D>().enabled = !gameObject.GetComponent<BoxCollider2D>().enabled;
    }
}
