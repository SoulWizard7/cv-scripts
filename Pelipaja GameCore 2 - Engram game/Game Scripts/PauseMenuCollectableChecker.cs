﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuCollectableChecker : MonoBehaviour
{
    GameObject player;
    GameManager gm;
    
    public GameObject collectable1;
    public GameObject collectable2;
    public GameObject collectable3;
    public GameObject collectable4;
    public GameObject collectable5;
    public GameObject collectable6;
    public GameObject collectable7;
    public GameObject collectable8;
    public GameObject collectable9;
    
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        gm = player.GetComponent<GameManager>();        
    }

    void Update()
    {
        if (gm.collectable1)
        {
            collectable1.SetActive(true);
        }
        else
        {
            collectable1.SetActive(false);
        }

        if (gm.collectable2)
        {
            collectable2.SetActive(true);
        }
        else
        {
            collectable2.SetActive(false);
        }

        if (gm.collectable3)
        {
            collectable3.SetActive(true);
        }
        else
        {
            collectable3.SetActive(false);
        }

        if (gm.collectable4)
        {
            collectable4.SetActive(true);
        }
        else
        {
            collectable4.SetActive(false);
        }

        if (gm.collectable5)
        {
            collectable5.SetActive(true);
        }
        else
        {
            collectable5.SetActive(false);
        }

        if (gm.collectable6)
        {
            collectable6.SetActive(true);
        }
        else
        {
            collectable6.SetActive(false);
        }

        if (gm.collectable7)
        {
            collectable7.SetActive(true);
        }
        else
        {
            collectable7.SetActive(false);
        }

        if (gm.collectable8)
        {
            collectable8.SetActive(true);
        }
        else
        {
            collectable8.SetActive(false);
        }

        if (gm.collectable9)
        {
            collectable9.SetActive(true);
        }
        else
        {
            collectable9.SetActive(false);
        }
    }
}
