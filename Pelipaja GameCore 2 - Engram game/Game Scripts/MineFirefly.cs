﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineFirefly : MonoBehaviour
{    
    public Transform target;

    [Range(0f, 1f)]
    [SerializeField]
    private float lerpSpeed = 0.1f;

    [SerializeField]
    private float followDistance = 1f;

    void LateUpdate()
    {
        Vector3 fromTargetToTransform = transform.position - target.position;
        if (fromTargetToTransform.magnitude > followDistance)
        {
            Vector3 distanceFromTarget = fromTargetToTransform.normalized * followDistance;

            Vector3 moveToWorldPosition = target.position + distanceFromTarget;

            transform.position = Vector3.Lerp(transform.position, moveToWorldPosition, lerpSpeed);
        }
    }
}
