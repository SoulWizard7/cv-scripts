﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingTrigger : MonoBehaviour
{
    private GameObject player;
    private GameManager gm;
   
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        gm = player.GetComponent<GameManager>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {  
            StartCoroutine(Delay());
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(10);
        gm.EndMenu();
    }
}
