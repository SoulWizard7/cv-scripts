﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    GameObject player;
    GameManager gm;    
    public GameObject particleSystem;
        
    public int collectableID = -1;

    public int sfxToPlay = 1;

    void Awake()
    {        
        player = GameObject.FindGameObjectWithTag("Player");
        gm = player.GetComponent<GameManager>();

        if (collectableID < 0)
        {
            Debug.LogError("wrong currentCollectableID on this gameObject" + this.gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == ("Player") && collectableID == 0)
        {
            Debug.Log("Picked up Collectable");
            gm.collectable1 = true;
            PlaySound();
        }
        else if (other.gameObject.tag == ("Player") && collectableID == 1)
        {
            Debug.Log("Picked up Collectable");
            gm.collectable2 = true;
            PlaySound();
        }
        else if (other.gameObject.tag == ("Player") && collectableID == 2)
        {
            Debug.Log("Picked up Collectable");
            gm.collectable3 = true;
            PlaySound();
        }
        else if (other.gameObject.tag == ("Player") && collectableID == 3)
        {
            Debug.Log("Picked up Collectable");
            gm.collectable4 = true;
            PlaySound();
        }
        else if (other.gameObject.tag == ("Player") && collectableID == 4)
        {
            Debug.Log("Picked up Collectable");
            gm.collectable5 = true;
            PlaySound();
        }
        else if (other.gameObject.tag == ("Player") && collectableID == 5)
        {
            Debug.Log("Picked up Collectable");
            gm.collectable6 = true;
            PlaySound();
        }
        else if (other.gameObject.tag == ("Player") && collectableID == 6)
        {
            Debug.Log("Picked up Collectable");
            gm.collectable7 = true;
            PlaySound();
        }
        else if (other.gameObject.tag == ("Player") && collectableID == 7)
        {
            Debug.Log("Picked up Collectable");
            gm.collectable8 = true;
            PlaySound();
        }
        else if (other.gameObject.tag == ("Player") && collectableID == 8)
        {
            Debug.Log("Picked up Collectable");
            gm.collectable9 = true;
            PlaySound();
        }       
    }

    void PlaySound()
    {
        AudioManager.instance.PlaySFX(sfxToPlay);
        this.gameObject.SetActive(false);
        Instantiate(particleSystem, transform.position, Quaternion.identity);
    }
}
