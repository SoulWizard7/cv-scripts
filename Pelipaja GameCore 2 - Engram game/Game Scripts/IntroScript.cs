﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroScript : MonoBehaviour
{
    public bool right = true;
    Rigidbody2D rb;
    public float movementSpeed = 3;
    public Animator animator;



    public GameObject blackOutSquare;
    public bool isFadedBlack;
    public float fadeTime;
    public Image blackScreen;

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

        blackScreen = blackOutSquare.GetComponent<Image>();

        Fade();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("isDead", false);
        float x = 1;
        animator.SetFloat("Speed", Mathf.Abs(x));
        if (right)
        {
            float moveRight = 1 * movementSpeed;

            rb.velocity = new Vector2(moveRight, rb.velocity.y);

        }
    }

    void Fade()
    {
        StartCoroutine(FadeToClear());
    }

    public IEnumerator FadeToClear()
    {
        blackOutSquare.gameObject.SetActive(true);
        blackScreen.color = Color.black;

        float rate = 1.0f / fadeTime;

        float progress = 0.0f;

        while (progress < 1.0f)
        {
            blackScreen.color = Color.Lerp(Color.black, Color.clear, progress);

            progress += rate * Time.deltaTime;

            yield return null;
        }

        blackScreen.color = Color.clear;
        blackOutSquare.gameObject.SetActive(false);
    }
}
