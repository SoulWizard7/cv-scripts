﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

//[RequireComponent(typeof(Light))]
public class SoftFlicker : MonoBehaviour
{
    public float minIntensity = 0.25f;
    public float maxIntensity = 0.5f;
    public float lerpSpeed = 1f;
    private float lerpValue;

    float random;
    Light2D light;

    void Start()
    {
        light = GetComponent<Light2D>();
        random = Random.Range(0.0f, 65535.0f);
    }

    void Update()
    {
        lerpValue += lerpSpeed * Time.deltaTime;
        //float noise = Mathf.PerlinNoise(random, Time.time);
        float noise = Mathf.PerlinNoise(random, lerpValue);
        light.intensity = Mathf.Lerp(minIntensity, maxIntensity, noise);
    }
}