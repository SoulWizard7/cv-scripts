﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boulder : MonoBehaviour
{
    GameObject player;    
    PlayerMovement pm;
    Rigidbody2D rb;
    
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        pm = player.GetComponent<PlayerMovement>();
        rb = GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == ("Player") && rb.angularVelocity < -30)
        {
            pm.Die();
        }/*
        if (col.gameObject.tag == ("Player") && rb.angularVelocity < +30)
        {
            pm.Die();
        }*/
    }
}
