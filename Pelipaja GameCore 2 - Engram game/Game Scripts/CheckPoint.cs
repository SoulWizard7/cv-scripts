﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class CheckPoint : MonoBehaviour
{

    public GameObject lt;

    GameObject player;
    GameManager gm;

    void Awake()
    {        
        player = GameObject.FindGameObjectWithTag("Player");
        gm = player.GetComponent<GameManager>();        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            gm.SavePlayer();
            Debug.Log("Game Saved");
            lt.SetActive(true);
        }
    }
}
