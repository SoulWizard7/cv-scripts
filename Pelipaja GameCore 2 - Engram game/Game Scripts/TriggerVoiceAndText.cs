﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerVoiceAndText : MonoBehaviour
{
    
    [SerializeField] GameObject voiceOverText;

    [SerializeField] GameObject textPosition;
    Transform textTransform;
    BoxCollider2D bc2D;
    public int voToPlay;

    
    void Start()
    {
        textPosition = GameObject.Find("TextPosition");
        textTransform = textPosition.transform;
        bc2D = GetComponent<BoxCollider2D>();
    }

    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            AudioManager.instance.PlayVoice(voToPlay);
            GameObject text = Instantiate(voiceOverText);
            text.transform.parent = textPosition.transform;
            text.transform.position = new Vector2(textTransform.position.x, textTransform.position.y);
            Destroy(text, 8f);
            bc2D.enabled = false;
        }
    }
}
