﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioSource[] bgm;
    public AudioSource[] sfx;
    public AudioSource[] voice;
    // Reference to Audio Source component
    private AudioSource audioSrc;

    // Music volume variable that will be modified
    // by dragging slider knob
    private float musicVolume = 1f;

    void Awake()
    {
        if (instance != null)
        {
            GameObject.Destroy(instance);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }

    // Use this for initialization
    void Start()
    {
        // Assign Audio Source component to control it
        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Setting volume option of Audio Source to be equal to musicVolume
        audioSrc.volume = musicVolume;
    }

    // Method that is called by slider game object
    // This method takes vol value passed by slider
    // and sets it as musicValue
    public void SetVolume(float vol)
    {
        musicVolume = vol;
    }


    public void PlayBGM(int musicToPlay)
    {
        if (!bgm[musicToPlay].isPlaying)
        {
            StopMusic();

            if (musicToPlay < bgm.Length)
            {
                bgm[musicToPlay].Play();
            }
        }
    }

    public void StopMusic()
    {
        for (int i = 0; i < bgm.Length; i++)
        {
            bgm[i].Stop();
        }
    }



    public void PlaySFX(int musicToPlay)
    {
        if (!sfx[musicToPlay].isPlaying)
        {
            StopSFX();

            if (musicToPlay < sfx.Length)
            {
                sfx[musicToPlay].Play();
            }
        }
    }

    public void StopSFX()
    {
        for (int i = 0; i < sfx.Length; i++)
        {
            sfx[i].Stop();
        }
    }




    public void PlayVoice(int musicToPlay)
    {
        if (!voice[musicToPlay].isPlaying)
        {
            StopVoice();

            if (musicToPlay < voice.Length)
            {
                voice[musicToPlay].Play();
            }
        }
    }

    public void StopVoice()
    {
        for (int i = 0; i < voice.Length; i++)
        {
            voice[i].Stop();
        }
    }

}
