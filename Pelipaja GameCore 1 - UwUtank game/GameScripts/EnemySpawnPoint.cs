﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnPoint : MonoBehaviour
{
    public Transform player;
    public float awakeDistance;
    public GameObject enemyPrefab;

    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) < awakeDistance)
        {
            Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
