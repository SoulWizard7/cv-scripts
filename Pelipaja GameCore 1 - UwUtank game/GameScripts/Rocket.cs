﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    public int damage = 60;
    public GameObject rocketHitEffect;
    public GameObject rocketHitSound;


    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        EnemyHealth enemy = hitInfo.GetComponent<EnemyHealth>();
        if (enemy != null)
        {
            enemy.TakeDamage(damage);
        }

        PlayerHealth player = hitInfo.GetComponent<PlayerHealth>();
        if (player != null)
        {
            player.TakeDamage(damage);
        }

        BossLonkero boss = hitInfo.GetComponent<BossLonkero>();
        if (boss != null)
        {
            boss.TakeDamage(damage);

        }
        Debug.Log("rocket did hit");
        GameObject effect = Instantiate(rocketHitEffect, transform.position, Quaternion.identity);        
        GameObject hitSound = Instantiate(rocketHitSound, transform.position, Quaternion.identity);        
        Destroy(hitSound, 3f);
        Destroy(effect, 5f);
        Destroy(gameObject);
    }

}
