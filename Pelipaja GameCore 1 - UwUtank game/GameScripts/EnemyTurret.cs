﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTurret : MonoBehaviour
{

    public float laserForce = 75f;
       
    public float shootDistance;
    public float awakeDistance;
    private Transform player;

    private float timeBetweenShots;
    public float startTimeBetweenShots;

    public GameObject bulletPrefab;
    public Transform firePoint;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        timeBetweenShots = startTimeBetweenShots;
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) > awakeDistance)  //kun pelaaja on kauempana kun vihu, vihu seisoo paikallaan. (pitää olla ensimmäisenä)
        {
            return;
        }


        //enemy rotates towards player
        Vector3 dir = player.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

        // enemy starts shooting first when inside shoot distance
        if (Vector2.Distance(transform.position, player.position) < shootDistance)
        {

            if (timeBetweenShots <= 0)
            {
                //Ampuu laserin kun timeBetweenShots aika menee nollaan.
                GameObject BulletVariantOne = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);                
                Rigidbody2D rb = BulletVariantOne.GetComponent<Rigidbody2D>();
                rb.AddForce(firePoint.up * laserForce, ForceMode2D.Impulse);
                AudioSource audioSource = GetComponent<AudioSource>();
                audioSource.Play();
                Destroy(BulletVariantOne, 5f);
                timeBetweenShots = startTimeBetweenShots;
            }
            else
            {
                timeBetweenShots -= Time.deltaTime;
            }
        }
    }
}
