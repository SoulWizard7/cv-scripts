﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectForce : MonoBehaviour
{
    ShootForce shootForce;

    AudioSource source;

    void Awake()
    {
        shootForce = FindObjectOfType<ShootForce>();
    }

    //poimii force ja antaa äänen
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == ("Player"))
        {
            Debug.Log("Picked up 10 seconds of force");
            PlaySound();
            ShootForce.totalAmmo += 10;
            shootForce.forceAmmoDisplay.text = ShootForce.totalAmmo.ToString();         //lähettä update HUD:iin
        }
    }

    void PlaySound()
    {
        source = GetComponent<AudioSource>();
        source.Play();
        GetComponent<SpriteRenderer>().enabled = false;             //disable sprite ja antaa 1f(1sekunti) aikaa ennenkuin destroy objekt...
        Destroy(gameObject, 1f);                                    //...jotta audio effekti ehtii soida
    }
}
