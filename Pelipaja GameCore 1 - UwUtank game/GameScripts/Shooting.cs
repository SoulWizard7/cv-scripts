﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    //vanha shooting skripti, eristetty 3 eri osaan = ShootRocket, ShootForce ja ShootLaser


    /*
    public Transform firePoint;
    public GameObject laserPrefab;
    public GameObject rocketPrefab;
    public GameObject smokeRocketPrefab;

    public float rocketForce = 15f;
    public float laserForce = 25f;
    public int damageForce = 40;

    public GameObject impactEffect;
    public LineRenderer lineRenderer;

    /*
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire2"))
        {
            lineRenderer.enabled = true;
        }
        else
        {
            lineRenderer.enabled = false;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
        
        if (Input.GetButtonDown("Fire4"))
        {
            ShootRocket();
        }
        
        if (Input.GetButtonDown("Fire2"))
        {
            ShootForce();
        }

    }

    void ShootRocket()
    {
        GameObject rocket = Instantiate(rocketPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D rocketrb = rocket.GetComponent<Rigidbody2D>();
        rocketrb.AddForce(firePoint.up * rocketForce, ForceMode2D.Impulse);
        Destroy(rocket, 5f);


    }




    void Shoot()
    {
        GameObject laser = Instantiate(laserPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = laser.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.up * laserForce, ForceMode2D.Impulse);
        Destroy(laser, 5f);
    }

    void ShootForce()
    {
        
        RaycastHit2D forceHit = Physics2D.Raycast(firePoint.position, firePoint.up);

        if (forceHit)
        {
            Debug.Log(forceHit.transform.name);
            Enemy enemy = forceHit.transform.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(damageForce);
            }

            GameObject lasereff = Instantiate(impactEffect, forceHit.point, Quaternion.identity);
            Destroy(lasereff, 3f);

            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.SetPosition(1, transform.InverseTransformPoint(forceHit.point));
        }
        else
        {
            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.SetPosition(1, Vector3.up * 10);
        }
    } 
    */
}
  