﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitching : MonoBehaviour
{
    public int selectedWeapon = 0;

    ShootLaser shootLaser;
    ShootRocket shootRocket;
    ShootForce shootForce;

    //ase hud highlight
    GameObject laserHighlight;
    GameObject rocketHighlight;
    GameObject forceHighlight;

    void Start()
    {
        laserHighlight = GameObject.Find("LaserHighlight");
        rocketHighlight = GameObject.Find("RocketHighlight");
        forceHighlight = GameObject.Find("ForceHighlight");


        laserHighlight.SetActive(true);
        rocketHighlight.SetActive(false);
        forceHighlight.SetActive(false);

        shootLaser = FindObjectOfType<ShootLaser>();
        shootLaser.laserAmmoDisplay.text = ShootLaser.totalAmmo.ToString();  //lähettä update HUD:iin

        shootRocket = FindObjectOfType<ShootRocket>();
        shootRocket.rocketAmmoDisplay.text = ShootRocket.totalAmmo.ToString();      //lähettä update HUD:iin

        shootForce = FindObjectOfType<ShootForce>();
        shootForce.forceAmmoDisplay.text = ShootForce.totalAmmo.ToString();         //lähettä update HUD:iin

        selectedWeapon = transform.childCount - 3;          //bug work-around

        SelectWeapon();

    }

    // Update is called once per frame
    void Update()
    {
        int previousSelectedWeapon = selectedWeapon;
        /*
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (selectedWeapon >= transform.childCount - 1)
                selectedWeapon = 0;
            else
                selectedWeapon++;
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (selectedWeapon <= transform.childCount - 1)
                selectedWeapon = 0;
            else
                selectedWeapon--;
        }*/

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            selectedWeapon = 0;
            laserHighlight.SetActive(true);
            rocketHighlight.SetActive(false);
            forceHighlight.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2) && transform.childCount >= 2)
        {
            selectedWeapon = 1;
            laserHighlight.SetActive(false);
            rocketHighlight.SetActive(true);
            forceHighlight.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3) && transform.childCount >= 3)
        {
            selectedWeapon = 2;
            laserHighlight.SetActive(false);
            rocketHighlight.SetActive(false);
            forceHighlight.SetActive(true);
        }
        /*
        if (Input.GetKeyDown(KeyCode.Alpha4) && transform.childCount >= 4)
        {
            selectedWeapon = 3;
        }*/

        if (previousSelectedWeapon != selectedWeapon)
        {
            SelectWeapon();
        }
    }
    void SelectWeapon()
    {
        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (i == selectedWeapon) weapon.gameObject.SetActive(true);
            else weapon.gameObject.SetActive(false);
            i++;
        }
    }
}
