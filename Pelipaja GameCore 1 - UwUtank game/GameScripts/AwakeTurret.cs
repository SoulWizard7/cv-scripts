﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeTurret : MonoBehaviour
{
    public Transform player;
    public float awakeDistance = 25;



    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }


    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) < awakeDistance)
        {
            GetComponent<EnemyTurret>().enabled = true;
        }
    }
}
