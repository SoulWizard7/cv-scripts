﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class EndingCastle : MonoBehaviour
{
    
    public ParticleSystem heartParticlesPrefab;
    public GameObject levelExit;
    public GameObject exitStop;

    // Start is called before the first frame update
    void Start()
    {
        exitStop = GameObject.Find("ExitStop");

        //exitStop.SetActive(false);

        levelExit = GameObject.Find("LevelExit");
        
        levelExit.SetActive(false);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Instantiate(heartParticlesPrefab, transform.position, quaternion.identity);

            exitStop.SetActive(true);

            StartCoroutine(CountDown());            
        }  
    }

    IEnumerator CountDown()
    {
        yield return new WaitForSeconds(13f);
        levelExit.SetActive(true);
    }

}
