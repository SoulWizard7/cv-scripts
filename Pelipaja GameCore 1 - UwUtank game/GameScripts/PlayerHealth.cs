﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{

    //skripti player health toiminnoista

    public int maxHealth = 300;
    public static int currentHealth;

    public GameObject deathEffect;

    public HealthBar healthBar;

    private Animator face;

    void Start()
    {
        face = GameObject.FindGameObjectWithTag("UwuFace").GetComponent<Animator>(); //koodi jotta löytää animaattori hierarkysta
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
        face.SetTrigger("GetHit");
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        face.SetTrigger("Die");
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);

        FindObjectOfType<GameManager>().GameOver();

    }
}
