﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShootRocket : MonoBehaviour
{
    public Transform firePoint;
    public GameObject rocketPrefab;
    public GameObject smokeRocketPrefab;

    public float rocketForce = 15f;

    //Rocket Ammo Data
    public int maxAmmoInClip = 1;                 //MaxAmmo in clip.
    private int currentAmmoInClip = -1;           //Ammo in clip. 
    public float reloadTime = 2f;
    private bool isReloading = false;       //Reload check
    public static int totalAmmo = 10;       //Ammunition amount


       //total ammo goes to UI (ei toimi, pitää vaihtaa rakettiin ennenkuin näkyy)
    public TextMeshProUGUI rocketAmmoDisplay;
        
    void Start()
    {
        if (currentAmmoInClip == -1) currentAmmoInClip = maxAmmoInClip;        
        rocketAmmoDisplay = GameObject.Find("RocketACTextPro").GetComponent<TextMeshProUGUI>();
    }

    void OnEnable()
    {
        isReloading = false;    
    }

    // Update is called once per frame
    void Update()
    {        
        rocketAmmoDisplay.text = totalAmmo.ToString();

        if (totalAmmo <= 0) return;

        if (isReloading) return;

        if (currentAmmoInClip <= 0)
        {
            StartCoroutine(Reload());
            return;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            ShootRockets();
        }

    }

    //Reloading for rocket weapon
    IEnumerator Reload()
    {
        isReloading = true;

        Debug.Log("Reloading rocket...");

        yield return new WaitForSeconds(reloadTime);

        currentAmmoInClip = maxAmmoInClip;

        isReloading = false;
    }

    void ShootRockets()
    {
        //ääni kun ampuu
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.Play();

        currentAmmoInClip--;
        totalAmmo--;

        //hakee rocket prefab ja ampuu sen
        GameObject rocket = Instantiate(rocketPrefab, firePoint.position, firePoint.rotation);        
        Rigidbody2D rocketrb = rocket.GetComponent<Rigidbody2D>();
        rocketrb.AddForce(firePoint.up * rocketForce, ForceMode2D.Impulse);
        Destroy(rocket, 5f);
    }
}
