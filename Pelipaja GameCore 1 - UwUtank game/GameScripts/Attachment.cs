﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attachment : MonoBehaviour
{
    public GameObject player;   //public variable pelaajasta

    private Vector3 offset;     //private variable joka laskee pelaajan ja kameran väliä
    
    // Start is called before the first frame update
    void Start()
    {
        //laskee pelaajan ja kameran positiot ja säilyttää ne offset variablessa
        offset = transform.position - player.transform.position;    
    }

    // LateUpdate tekee kaiken Updaten jälkeen
    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
}
