﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLonkero : MonoBehaviour
{
    public Transform homingFirePoint1;
    public Transform homingFirePoint2;

    private float laserForce = 30f;

    public GameObject homingMissilePrefab;

    public float timeBetweenMissileShots;
    public float startTimeBetweenMissileShots;

    public float timeBetweenLaserShots;
    public float startTimeBetweenLaserShots;

    public float timeBetweenBulletShots;
    public float startTimeBetweenBulletShots;

    public Transform player;

    public int missileTimes = 3;
    public int laserTimes = 0;
    public int bulletTimes = 0;

    //public GameObject laserPrefab;

    public Transform laserFirePoint1;
    public Transform laserFirePoint2;
    public Transform laserFirePoint3;
    public Transform laserFirePoint4;
    public Transform laserFirePoint5;
    public Transform laserFirePoint6;
    public Transform laserFirePoint7;
    public Transform laserFirePoint8;

    public float radiusShoot = 10f;
    public float moveSpeed = 35f;

    public int health = 1000;

    public GameObject deathEffect;
    public GameObject tankExplosionSound;
    public GameObject holeSprite;

    public float timeBetweenExplosion;
    public float startTimeBetweenExplosion;
    public int explosionTimes = 0;

    public GameObject exitStop;
    

    public Animator face;

    // circulation movement
    private float rotateSpeed = 4f;
    private float inverseRotateSpeed = -7f;
    private float radius = 4f;
    private Vector2 _centre;
    private float _angle;

    [SerializeField]
    int numberOfBullets;
    [SerializeField]
    GameObject bulletPrefab;
    [SerializeField]
    int numberOfLasers;
    [SerializeField]
    GameObject laserPrefab;

    public int musicToPlay;
    public int musicToPlayEnd;
    private bool musicStarted;

    public Vector2 startPoint;

    public float awakeDistance;

    void Start()
    {
        exitStop = GameObject.Find("ExitStop");

        face = GameObject.FindGameObjectWithTag("UwuFace").GetComponent<Animator>(); //koodi jotta löytää animaattori hierarkysta

        _centre = transform.position;

        timeBetweenMissileShots = startTimeBetweenMissileShots;

        timeBetweenLaserShots = startTimeBetweenLaserShots;

        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) > awakeDistance)  //kun pelaaja on kauempana kun vihu, vihu seisoo paikallaan. (pitää olla ensimmäisenä)
        {
            return;
        }

         // Tämä on musiikkienabler bossimusiikille
            if(!musicStarted)
            {
                musicStarted = true;
                AudioManager.instance.PlayBGM(musicToPlay);
                exitStop.SetActive(false);
            }
        
        startPoint = transform.position;
       
        if (laserTimes > 0)
        {
            //boss circulation
            _angle += rotateSpeed * Time.deltaTime;
            var offset = new Vector2(Mathf.Sin(_angle), Mathf.Cos(_angle)) * radius;
            transform.position = _centre + offset;
        }

        if (bulletTimes > 0)
        {
            //boss reverse circulation
            _angle += inverseRotateSpeed * Time.deltaTime;
            var offset = new Vector2(Mathf.Sin(_angle), Mathf.Cos(_angle)) * radius;
            transform.position = _centre + offset;
        }

        if (missileTimes > 0)
        {
            //enemy looks at player
            Vector3 dir = player.position - transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle + 270, Vector3.forward);
        }

        if (timeBetweenMissileShots <= 0 && missileTimes > 0)
        {
            //Ampuu missile kun timeBetweenShots aika menee nollaan.
            GameObject missile1 = Instantiate(homingMissilePrefab, homingFirePoint1.position, homingFirePoint1.rotation);

            GameObject missile2 = Instantiate(homingMissilePrefab, homingFirePoint2.position, homingFirePoint2.rotation);

            AudioSource audioSource = GetComponent<AudioSource>();
            audioSource.Play();

            Destroy(missile1, 15f);
            Destroy(missile2, 15f);
            timeBetweenMissileShots = startTimeBetweenMissileShots;
            missileTimes--;

            if (missileTimes == 0)
            {
                laserTimes = +10;
                timeBetweenLaserShots = 2;
            }
        }
        
        if (timeBetweenLaserShots <= 0 && laserTimes > 0)
        {
            GameObject laser1 = Instantiate(laserPrefab, laserFirePoint1.position, laserFirePoint1.rotation);
            Rigidbody2D rb1 = laser1.GetComponent<Rigidbody2D>();
            rb1.AddForce(laserFirePoint1.up * laserForce, ForceMode2D.Impulse);
            Destroy(laser1, 5f);

            GameObject laser2 = Instantiate(laserPrefab, laserFirePoint2.position, laserFirePoint2.rotation);
            Rigidbody2D rb2 = laser2.GetComponent<Rigidbody2D>();
            rb2.AddForce(laserFirePoint2.up * laserForce, ForceMode2D.Impulse);
            Destroy(laser2, 5f);

            GameObject laser3 = Instantiate(laserPrefab, laserFirePoint3.position, laserFirePoint3.rotation);
            Rigidbody2D rb3 = laser3.GetComponent<Rigidbody2D>();
            rb3.AddForce(laserFirePoint3.up * laserForce, ForceMode2D.Impulse);
            Destroy(laser3, 5f);

            GameObject laser4 = Instantiate(laserPrefab, laserFirePoint4.position, laserFirePoint4.rotation);
            Rigidbody2D rb4 = laser4.GetComponent<Rigidbody2D>();
            rb4.AddForce(laserFirePoint4.up * laserForce, ForceMode2D.Impulse);
            Destroy(laser4, 5f);

            GameObject laser5 = Instantiate(laserPrefab, laserFirePoint5.position, laserFirePoint5.rotation);
            Rigidbody2D rb5 = laser5.GetComponent<Rigidbody2D>();
            rb5.AddForce(laserFirePoint5.up * laserForce, ForceMode2D.Impulse);
            Destroy(laser5, 5f);

            GameObject laser6 = Instantiate(laserPrefab, laserFirePoint6.position, laserFirePoint6.rotation);
            Rigidbody2D rb6 = laser6.GetComponent<Rigidbody2D>();
            rb6.AddForce(laserFirePoint6.up * laserForce, ForceMode2D.Impulse);
            Destroy(laser6, 5f);

            GameObject laser7 = Instantiate(laserPrefab, laserFirePoint7.position, laserFirePoint7.rotation);
            Rigidbody2D rb7 = laser7.GetComponent<Rigidbody2D>();
            rb7.AddForce(laserFirePoint7.up * laserForce, ForceMode2D.Impulse);
            Destroy(laser7, 5f);

            GameObject laser8 = Instantiate(laserPrefab, laserFirePoint8.position, laserFirePoint8.rotation);
            Rigidbody2D rb8 = laser8.GetComponent<Rigidbody2D>();
            rb8.AddForce(laserFirePoint8.up * laserForce, ForceMode2D.Impulse);
            Destroy(laser8, 5f);

            //SpawnLasers(numberOfLasers);

            timeBetweenLaserShots = startTimeBetweenLaserShots;
            laserTimes--;

            if (laserTimes == 0)
            {
                bulletTimes = +8;
                timeBetweenBulletShots = 1;
            }
        }

        if (timeBetweenBulletShots <= 0 && bulletTimes > 0)
        {

            SpawnBullets(numberOfBullets);

            timeBetweenBulletShots = startTimeBetweenBulletShots;
            bulletTimes--;

            if (bulletTimes == 0)
            {
                missileTimes = +4;
                timeBetweenMissileShots = 3;
            }
        }

        if (explosionTimes > 0 && timeBetweenExplosion <= 0)
        {
            Instantiate(deathEffect, _centre + Random.insideUnitCircle * 9, Quaternion.identity);
            timeBetweenExplosion = startTimeBetweenExplosion;
            explosionTimes--;
        }

        else
        {
            timeBetweenMissileShots -= Time.deltaTime;
            timeBetweenLaserShots -= Time.deltaTime;
            timeBetweenBulletShots -= Time.deltaTime;
            timeBetweenExplosion -= Time.deltaTime;
        }
        
    }

    void SpawnLasers(int numberOfLasers)
    {
        float angleStep = 360f / numberOfLasers;
        float angle = 0f;

        for (int i = 0; i <= numberOfLasers - 1; i++)
        {
            float projectileDirXposition = startPoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * radiusShoot;
            float projectileDirYposition = startPoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * radiusShoot;

            Vector2 projectileVector = new Vector2(projectileDirXposition, projectileDirYposition);
            Vector2 projectileMoveDirection = (projectileVector - startPoint).normalized * moveSpeed;

            var proj = Instantiate(laserPrefab, startPoint + (projectileMoveDirection * 0.1f), Quaternion.identity);
            proj.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileMoveDirection.x, projectileMoveDirection.y);

            angle += angleStep;
        }
    }

    void SpawnBullets(int numberOfBullets)
    {
        float angleStep = 360f / numberOfBullets;
        float angle = 0f;

        for (int i = 0; i <= numberOfBullets - 1; i++)
        {
            float projectileDirXposition = startPoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * radiusShoot;
            float projectileDirYposition = startPoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * radiusShoot;

            Vector2 projectileVector = new Vector2(projectileDirXposition, projectileDirYposition);
            Vector2 projectileMoveDirection = (projectileVector - startPoint).normalized * moveSpeed;

            var proj = Instantiate(bulletPrefab, startPoint + (projectileMoveDirection * 0.1f), Quaternion.identity);
            proj.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileMoveDirection.x, projectileMoveDirection.y);

            angle += angleStep;
        }
    }
        
    public void TakeDamage(int damage)
    {
        health -= damage;
        face.SetTrigger("HitEnemy");      //UI face animator

        if (health <= 0)
        {
            Die();
        }
    }
    void Die()
    {
        AudioManager.instance.PlayBGM(musicToPlayEnd);        
        explosionTimes = +250;
        Destroy(gameObject, 4f);
        GameObject tankExp = Instantiate(tankExplosionSound, transform.position, Quaternion.identity);
        Destroy(tankExp, 3f);
        exitStop.SetActive(false);
        Instantiate(holeSprite, transform.position, Quaternion.identity);
    }

}
