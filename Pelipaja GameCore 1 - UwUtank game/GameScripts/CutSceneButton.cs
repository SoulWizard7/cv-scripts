﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CutSceneButton : MonoBehaviour
{
    Animator animator;
    public GameObject cutSceneButton;
    public float timeToWait;

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        cutSceneButton.SetActive(false);
        StartCoroutine(SceneAnimation());
    }

    IEnumerator SceneAnimation()
    {
        yield return new WaitForSeconds(timeToWait);
        cutSceneButton.SetActive(true);
    }
}
