﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    //Entinen Enemy Skripti on nyt tämä EnemyHealth skripti
    

    public int health = 100;

    public GameObject deathEffect;
    public GameObject tankExplosionSound;
    public GameObject holeSprite;

    public Animator face;

    void Start()
    {
        face = GameObject.FindGameObjectWithTag("UwuFace").GetComponent<Animator>(); //koodi jotta löytää animaattori hierarkysta
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        face.SetTrigger("HitEnemy");      //UI face animator


        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        GameObject tankExp = Instantiate(tankExplosionSound, transform.position, Quaternion.identity);
        Destroy(tankExp, 3f);
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Instantiate(holeSprite, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}