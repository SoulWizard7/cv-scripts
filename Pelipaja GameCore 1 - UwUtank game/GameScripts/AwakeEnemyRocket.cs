﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeEnemyRocket : MonoBehaviour
{
    public Transform player;
    public float awakeDistance;



    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }


    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) < awakeDistance)
        {
            GetComponent<EnemyRocketShooting>().enabled = true;
        }
    }
}
