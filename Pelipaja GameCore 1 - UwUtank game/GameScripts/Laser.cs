﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public int damage = 40;
    public GameObject laserHitEffect;
    public GameObject laserHitSound;

    
    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        EnemyHealth enemy = hitInfo.GetComponent<EnemyHealth>();
        if (enemy != null)
        {
            enemy.TakeDamage(damage);
            
        }

        PlayerHealth player = hitInfo.GetComponent<PlayerHealth>();
        if (player != null)
        {
            player.TakeDamage(damage);
        }

        BossLonkero boss = hitInfo.GetComponent<BossLonkero>();
        if (boss != null)
        {
            boss.TakeDamage(damage);

        }

        Debug.Log("laser did hit");

        GameObject effect = Instantiate(laserHitEffect, transform.position, Quaternion.identity);
        GameObject hitSound = Instantiate(laserHitSound, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        Destroy(hitSound, 2f);
        Destroy(gameObject);
    }

}
