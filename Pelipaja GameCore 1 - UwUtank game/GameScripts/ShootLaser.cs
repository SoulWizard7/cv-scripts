﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShootLaser : MonoBehaviour
{
    public Transform firePoint;
    public GameObject laserPrefab;
        
    public float laserForce = 25f;

    //fire rate
    public float fireRate = 15f;
    private float nextTimeToFire = 0f;

    //reloading
    public int maxAmmoInClip = 3;              //MaxAmmo in clip.
    public int currentAmmoInClip = -1;          //Ammo in clip. 
    public float reloadTime = 2f;
    private bool isReloading = false;           //Reload check
    public static int totalAmmo = 20;                  //Ammunition amount

    //total ammo goes to UI
    public TextMeshProUGUI laserAmmoDisplay;

    void Start()
    {
        if (currentAmmoInClip == -1) currentAmmoInClip = maxAmmoInClip;
        
        laserAmmoDisplay = GameObject.Find("LaserACTextPro").GetComponent<TextMeshProUGUI>();
    }

    void OnEnable()
    {
        isReloading = false;
    }

    // Update is called once per frame
    void Update()
    {
        laserAmmoDisplay.text = totalAmmo.ToString();
        
        if (totalAmmo <= 0) return;

        if (isReloading) return;

        if (currentAmmoInClip <= 0)
        {
            StartCoroutine(Reload());
            return;
        }

        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }
    }

    IEnumerator Reload()
    {
        isReloading = true;

        Debug.Log("Reloading laser...");

        yield return new WaitForSeconds(reloadTime);

        currentAmmoInClip = maxAmmoInClip;

        isReloading = false;
    }

    void Shoot()
    {
        //ääni kun ampuu
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.Play();

        currentAmmoInClip--;
        totalAmmo--;

        GameObject laser = Instantiate(laserPrefab, firePoint.position, firePoint.rotation);        
        Rigidbody2D rb = laser.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.up * laserForce, ForceMode2D.Impulse);
        Destroy(laser, 5f);
    }
}