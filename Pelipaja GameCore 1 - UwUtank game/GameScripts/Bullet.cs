﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int damage = 10;
    public GameObject bulletHitEffect;
    public GameObject bulletHitSound;


    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        EnemyHealth enemy = hitInfo.GetComponent<EnemyHealth>();
        if (enemy != null)
        {
            enemy.TakeDamage(damage);

        }

        PlayerHealth player = hitInfo.GetComponent<PlayerHealth>();
        if (player != null)
        {
            player.TakeDamage(damage);
        }

        Debug.Log("bullet did hit");

        GameObject effect = Instantiate(bulletHitEffect, transform.position, Quaternion.identity);
        GameObject hitSound = Instantiate(bulletHitSound, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        Destroy(hitSound, 2f);
        Destroy(gameObject);
    }

}
