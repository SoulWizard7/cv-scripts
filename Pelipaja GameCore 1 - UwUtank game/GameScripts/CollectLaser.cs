﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectLaser : MonoBehaviour
{
    ShootLaser shootLaser;   

    AudioSource source;

    void Awake()
    {
        shootLaser = FindObjectOfType<ShootLaser>();        
    }

    //poimii laaserit ja antaa äänen
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == ("Player"))
        {
            Debug.Log("Picked up 10 laser");
            PlaySound();
            ShootLaser.totalAmmo += 10;
            shootLaser.laserAmmoDisplay.text = ShootLaser.totalAmmo.ToString();  //lähettä update HUD:iin
        }
    }

    void PlaySound()
    {
        source = GetComponent<AudioSource>();
        source.Play();
        GetComponent<SpriteRenderer>().enabled = false;         //disable sprite ja antaa 1f(1sekunti) aikaa ennenkuin destroy objekt...
        Destroy(gameObject, 1f);                                //...jotta audio effekti ehtii soida
    }
}
