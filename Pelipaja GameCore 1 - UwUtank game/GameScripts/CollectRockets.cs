﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectRockets : MonoBehaviour
{
    ShootRocket shootRocket;

    AudioSource source;

    void Awake()
    {
        shootRocket = FindObjectOfType<ShootRocket>();
    }


    //poimii 5 rockets ja antaa äänen
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == ("Player"))
        {
            Debug.Log("Picked up 10 rockets");
            PlaySound();
            ShootRocket.totalAmmo += 10;
            shootRocket.rocketAmmoDisplay.text = ShootRocket.totalAmmo.ToString();      //lähettä update HUD:iin
        }
    }

    void PlaySound()
    {
        source = GetComponent<AudioSource>();
        source.Play();
        GetComponent<SpriteRenderer>().enabled = false;        //disable sprite ja antaa 1f(1sekunti) aikaa ennenkuin destroy objekt...
        Destroy(gameObject, 1f);                                //...jotta audio effekti ehtii soida
    }
}
