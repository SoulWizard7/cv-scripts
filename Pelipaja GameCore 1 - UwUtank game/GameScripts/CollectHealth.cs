﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectHealth : MonoBehaviour
{
    PlayerHealth playerHealth;

    AudioSource source;

    private int healAmount = 300;

    void Awake()
    {
        playerHealth = FindObjectOfType<PlayerHealth>();
    }


    //poimii force ja antaa äänen
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == ("Player"))
        {
            Debug.Log("Picked up 10 seconds of force");
            PlaySound();
            PlayerHealth.currentHealth = healAmount;
            playerHealth.healthBar.SetHealth(PlayerHealth.currentHealth);
            
        }
    }

    void PlaySound()
    {
        source = GetComponent<AudioSource>();
        source.Play();
        GetComponent<SpriteRenderer>().enabled = false;
        Destroy(gameObject, 1f);
    }
}
