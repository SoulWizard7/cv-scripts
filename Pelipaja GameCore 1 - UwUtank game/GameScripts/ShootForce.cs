﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class ShootForce : MonoBehaviour
{
    public Transform firePoint;
    
    public int damageForce = 50;
    public float damTimer;              //force on nyt Damage Per Second

    public new AudioSource audio;

    public GameObject impactEffect;
    public LineRenderer lineRenderer;

    public float maxShootDistance = 22;

    public static int totalAmmo = 10;            //Total ammo
        
    public TextMeshProUGUI forceAmmoDisplay;

    //ongelma! force ase ei mene läpi pick-up

    void Start()
    {        
        forceAmmoDisplay = GameObject.Find("ForceACTextPro").GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        forceAmmoDisplay.text = totalAmmo.ToString();
        
        if (totalAmmo <= 0)
        {
            AudioSource audioSource = GetComponent<AudioSource>();
            audioSource.Stop();
            lineRenderer.enabled = false;
            return;
        }
        if (damTimer > 0)
        {
            damTimer -= Time.deltaTime;
        }
        
        if (Input.GetButton("Fire1"))
        {
            lineRenderer.enabled = true;
            audio.enabled = true;
        }

        else
        {
            lineRenderer.enabled = false;
            audio.enabled = false;
        }

        if (Input.GetButton("Fire1"))
        {
            ShootForces();
        }
    }
    void ShootForces()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            //ääni kun ampuu
            AudioSource audioSource = GetComponent<AudioSource>();
            audioSource.Play();
        }

            //käytetään raycast force aseen, en ymmärrä, kopioitu Brackeys youtubesta(Niklas)
        RaycastHit2D forceHit = Physics2D.Raycast(firePoint.position, firePoint.up);

        if (forceHit)
        {
            if (Vector2.Distance(forceHit.transform.position, firePoint.position) < maxShootDistance)
            {
                Debug.Log(forceHit.transform.name);
                EnemyHealth enemy = forceHit.transform.GetComponent<EnemyHealth>();
                if (enemy != null)
                {
                    if (damTimer <= 0)
                    {
                        enemy.TakeDamage(damageForce);
                    }
                    if (damTimer <= 0)
                    {
                        damTimer += 1;
                        totalAmmo--;
                    }
                }
                BossLonkero boss = forceHit.transform.GetComponent<BossLonkero>();
                if (boss != null)
                {
                    
                    if (damTimer <= 0)
                    {
                        boss.TakeDamage(damageForce);
                    }
                    if (damTimer <= 0)
                    {
                        damTimer += 1;
                        totalAmmo--;
                    }
                }

                GameObject lasereff = Instantiate(impactEffect, forceHit.point, Quaternion.identity);
                Destroy(lasereff, 1f);

                lineRenderer.SetPosition(0, Vector3.zero);
                lineRenderer.SetPosition(1, firePoint.InverseTransformPoint(forceHit.point));
            }
        }
        else
        {
            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.SetPosition(1, Vector3.up * 22);
        }
    }
}
