﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Canvas Menus")]
    [SerializeField] private GameObject mainMenuUI;
    [SerializeField] private GameObject pauseMenuUI;
    [SerializeField] private GameObject HUD_UI;
    [SerializeField] private GameObject gameOverUI;
    
    public static bool GameIsPaused = false;

    private bool gameHasEnded;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
        SceneSelection();
    }

#region MainMenu
    //GameOver State after death
    public void GameOver ()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("GAME OVER");
            gameOverUI.SetActive(true);
        }
    } 

    //Restart the level
    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    //
    public void SceneSelection()
    {
        if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName ("Menu"))
        {
            mainMenuUI.gameObject.SetActive(true);
        }
        else
        {
            mainMenuUI.gameObject.SetActive(false);
        }
    }

#endregion
#region PauseMenu
    //Resumes the paused game
    public void Resume()
    {
        Debug.Log("Resume button is working");
        Time.timeScale = 1;
        pauseMenuUI.SetActive(false);
        GameIsPaused = false;
    }

    //Pauses the game
    void Pause ()
    {
        
        Time.timeScale = 0;
        pauseMenuUI.SetActive(true);
        
        GameIsPaused = true;
    }

    //Loads the Menu, ends the game
    public void LoadMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }
   
   //Quits the application
    public void QuitGame()
    {
        Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }
#endregion
}
