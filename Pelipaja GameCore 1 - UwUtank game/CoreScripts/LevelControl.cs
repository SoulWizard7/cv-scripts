﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelControl : MonoBehaviour
{
    //public int index;
    //public string levelName;


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //Play next level on buildIndex
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

            //Loading level with build index
            //SceneManager.LoadScene(index);

            //Loading level with scene name
            //SceneManager.LoadScene(levelName);

            //Restart level
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

            //audioManager.StopMusic();
        }
    }

    public void PlayNextLevel()
    {
        //Play next level on buildIndex
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        //audioManager.StopMusic();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

}
